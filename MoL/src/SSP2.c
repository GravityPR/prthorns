 /*@@
   @file      SSP2.c
   @date      Mon May 22, 2023
   @author    Kevin Franceschetti, Roberto De Pietri
   @desc 
   A routine to perform SSP2(2,2,2) evolution. Mostly copied from
   RK2.c and GenericRK.c
   @enddesc 
   @version   $Header$
 @@*/

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

#include "ExternalVariables.h"
#include "Operators.h"

#include <math.h>

static const char *rcsid = "$Header$";

CCTK_FILEVERSION(CactusBase_MoL_SSP2_c);

/********************************************************************
 *********************     Local Data Types   ***********************
 ********************************************************************/

/********************************************************************
 ********************* Local Routine Prototypes *********************
 ********************************************************************/

/********************************************************************
 ***************** Scheduled Routine Prototypes *********************
 ********************************************************************/

void MoL_SSP2Add(CCTK_ARGUMENTS);

/********************************************************************
 ********************* Other Routine Prototypes *********************
 ********************************************************************/

/********************************************************************
 *********************     Local Data   *****************************
 ********************************************************************/

/********************************************************************
 *********************     External Routines   **********************
 ********************************************************************/

 /*@@
   @routine    MoL_SSP2Add
   @date       
   @author     
   @desc 
   Performs a single step of a SSP2(2,2,2) type time
   integration.
   @enddesc 
   @calls     
   @calledby   
   @history 
 
   @endhistory 

@@*/

void MoL_SSP2Add(CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS_CHECKED(MoL_SSP2Add);
  DECLARE_CCTK_PARAMETERS;
    
  CCTK_INT const scratchspace_firstindex = CCTK_FirstVarIndex("MOL::SCRATCHSPACE");
  CCTK_INT const mol_step = MoL_Intermediate_Steps - (*MoL_Intermediate_Step);
  CCTK_REAL const dt = (*Original_Delta_Time) / cctkGH->cctk_timefac;

  /* Real GFs */
  
  CCTK_INT rl = 0;
  if (CCTK_IsFunctionAliased("GetRefinementLevel")) {
    rl = GetRefinementLevel(cctkGH);
  }
  CCTK_INT tl = 0;
  
  switch (mol_step)
  {
    case 0:
      {
        for (CCTK_INT var = 0; var < MoLNumEvolvedVariables; var++)
        {
	  MoL_LinearCombination(cctkGH,
                                EvolvedVariableIndex[var], rl, tl, 1.0,
                                NULL, NULL, NULL, 0);
	
	  /* scratch storage */
	  MoL_LinearCombination(cctkGH,
                                scratchspace_firstindex, rl, var, 0.0,
                                NULL, NULL, NULL, 0);
        }
        break;
      }
    case 1:
      {
        for (CCTK_INT var = 0; var < MoLNumEvolvedVariables; var++)
        {
	  {
            CCTK_INT const nsrcs = 2;
            CCTK_INT const srcs[] =
              {EvolvedVariableIndex[var], RHSVariableIndex[var]};
            CCTK_INT const tls[] = {1, 0};
            CCTK_REAL const facts[] = {3.0, dt};
            MoL_LinearCombination(cctkGH,
                                  EvolvedVariableIndex[var], rl, tl, -2.0,
                                  srcs, tls, facts, nsrcs);
	  }
	  
	  /* scratch storage */
	  {
            CCTK_INT const nsrcs = 1;
            CCTK_INT const srcs[] = {RHSVariableIndex[var]};
            CCTK_INT const tls[] = {0};
            CCTK_REAL const facts[] = {0.5*dt};
	    MoL_LinearCombination(cctkGH,
                                  scratchspace_firstindex, rl, var, 0.0,
                                  srcs, tls, facts, nsrcs);
	  }
	}
        break;
      }
    case 2:
      {
        for (CCTK_INT var = 0; var < MoLNumEvolvedVariables; var++)
        {
          CCTK_INT const nsrcs = 3;
          CCTK_INT const srcs[] =
            {EvolvedVariableIndex[var], scratchspace_firstindex, RHSVariableIndex[var]};
          CCTK_INT const tls[] = {1, var, 0};
          CCTK_REAL const facts[] = {1.0, 1.0, 0.5*dt};
          MoL_LinearCombination(cctkGH,
                                EvolvedVariableIndex[var], rl, tl, 0.0,
                                srcs, tls, facts, nsrcs);
	}
        break;
      }
    default:
      CCTK_VERROR("Internal error. Unexpected substep %d", (int)mol_step);
      break;
  }
  
  /* Real arrays */

  CCTK_INT arrayscratchlocation = 0;

  for (CCTK_INT var = 0; var < MoLNumEvolvedArrayVariables; var++)
  {

    CCTK_REAL *restrict UpdateVar = (CCTK_REAL *)CCTK_VarDataPtrI(
        cctkGH, 0, EvolvedArrayVariableIndex[var]);
    CCTK_REAL *restrict OldVar = (CCTK_REAL *)CCTK_VarDataPtrI(
        cctkGH, 1, EvolvedArrayVariableIndex[var]);
    CCTK_REAL const *restrict RHSVar = (CCTK_REAL const *)CCTK_VarDataPtrI(
        cctkGH, 0, RHSArrayVariableIndex[var]);
    
    cGroupDynamicData arraydata;
    CCTK_INT const groupindex = CCTK_GroupIndexFromVarI(EvolvedArrayVariableIndex[var]);
    CCTK_INT const ierr = CCTK_GroupDynamicData(cctkGH, groupindex,
                                 &arraydata);
    if (ierr)
    {
      CCTK_VWarn(0, __LINE__, __FILE__, CCTK_THORNSTRING, 
                 "The driver does not return group information "
                 "for group '%s'.", 
                 CCTK_GroupName(groupindex));
    }
    
    CCTK_INT arraytotalsize = 1;
    for (CCTK_INT arraydim = 0; arraydim < arraydata.dim; arraydim++)
    {
      arraytotalsize *= arraydata.ash[arraydim];
    }

    CCTK_REAL *restrict ScratchVar = &ArrayScratchSpace[arrayscratchlocation];

    if (mol_step == 0) {
#pragma omp /*parallel for*/ simd
      for (CCTK_INT index = 0; index < arraytotalsize; index++)
      {
        UpdateVar[index] = OldVar[index];
	ScratchVar[index] = 0;
      }
    }

    if (mol_step == 1) {
#pragma omp /*parallel for*/ simd
      for (CCTK_INT index = 0; index < arraytotalsize; index++)
      {
	UpdateVar[index] = 3.0 * OldVar[index] - 2.0 * UpdateVar[index] + dt * RHSVar[index];
	ScratchVar[index] = 0.5 * dt * RHSVar[index];
      }
    }
    
    if (mol_step == 2) {
#pragma omp /*parallel for*/ simd
      for (CCTK_INT index = 0; index < arraytotalsize; index++)
      {
	UpdateVar[index] = OldVar[index] + ScratchVar[index] + 0.5 * dt * RHSVar[index];
      }
    }
    
    arrayscratchlocation += arraytotalsize;
  }

  /* Coefficient for the implicit step
   * The coefficient is set to zero at the last step to avoid the implicit step
   */
  switch (mol_step)
  {
    case 2:
      (*RKImexCoefficient) = 0.0;
      break;
    default:
      (*RKImexCoefficient) = 1.0 - 1.0 / sqrt(2.0);
      break;
  }

  return;
}


