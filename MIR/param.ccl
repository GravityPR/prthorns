#================================================================#
# Parameter definitions for thorn MIR                            #
#================================================================#


######################################################
########## PARAMETERS SHARED FROM HYDROBASE ##########
######################################################
shares: HydroBase

USES CCTK_INT timelevels
USES STRING prolongation_type

EXTENDS KEYWORD evolution_method
{
  "MIR" :: "Use MIR to evolve the hydro variables"
}

EXTENDS KEYWORD Bvec_evolution_method
{
  "MIR" :: "Evolve the magnetic field using MIR"
}


######################################################
######### PARAMETERS SHARED FROM ELECTROBASE #########
######################################################
shares: ElectroBase

### Initial data ###
EXTENDS KEYWORD initial_Evec
{
  "MIR" :: "Evaluate the initial electric field using the IMHD relation"
}

EXTENDS KEYWORD initial_Echarge
{
  "MIR" :: "Evaluate the initial electric charge using MIR"
}

EXTENDS KEYWORD initial_Jvec
{
  "MIR" :: "Evaluate the initial electric current using MIR"
}

EXTENDS KEYWORD initial_eta_ohm
{
  "MIR" :: "Evaluate the initial eta_ohm using MIR"
}

### Evolution methods ###
EXTENDS KEYWORD Evec_evolution_method
{
  "kin_fromS" :: "Evaluate the electric field from the momentum S in the kinematic approximation"
  "IMHD"      :: "Ideal MHD"
  "Ohm"       :: "Ohm's law (Resistive MHD)"
}

EXTENDS KEYWORD Echarge_evolution_method
{
  "finite_differences" :: "Evaluate the electric charge using Central Finite Differences"
  "flux"               :: "Evaluate the electric charge as flux"
}

EXTENDS KEYWORD eta_ohm_evolution_method
{
  "constant" :: "Constant electric resistivity within the star"
}


######################################################
############# PARAMETERS SHARED FROM MOL #############
######################################################
shares: MethodOfLines

USES CCTK_INT MoL_Num_Evolved_Vars
USES CCTK_INT MoL_Num_Evolved_Vars_Slow
USES CCTK_INT MoL_Num_Constrained_Vars
USES CCTK_INT MoL_Num_SaveAndRestore_Vars
USES CCTK_INT MoL_Max_Evolved_Array_Size 
USES CCTK_INT MoL_Num_ArrayEvolved_Vars


######################################################
########## PARAMETERS SHARED FROM SPACEMASK ##########
######################################################
shares:SpaceMask

USES boolean use_mask


#================================================================#
RESTRICTED:


######################################################
###### PARAMETERS FOR THE RECONSTRUCTION METHOD ######
######################################################
KEYWORD recon_method "Which reconstruction method to use" STEERABLE=RECOVER
{
  "tvd"    :: "Slope limited TVD reconstruction"
  "weno-z" :: "WENO-Z reconstruction"
} "weno-z"

KEYWORD tvd_limiter "Which slope limiter to use"
{
  "mc2"    :: "Van Leer MC - Luca"
  "minmod" :: "Minmod"
} "minmod"


######################################################
######### PARAMETERS FOR THE RIEMANN SOLVER ##########
######################################################
KEYWORD flux_type "Which Riemann solver to use" STEERABLE=RECOVER
{
  "HLLE"  :: "HLLE flux"
  "LF"    :: "Lax-Friedrichs flux"
} "HLLE"


######################################################
############### PARAMETERS FOR THE EOS ###############
######################################################
KEYWORD eos_type "Type of EoS"
{
  "Ideal_Fluid" :: "Ideal fluid"
  "Piecewise"   :: "Piecewise polytropic"
  "Taub"        :: "Taub"
  "Tabulated"   :: "Tabulated EoS"
} "Ideal_Fluid"

BOOLEAN force_taub_inequality ""
{
} "no"

STRING eos_table "Name of the tabulated EoS"
{
  .* :: "Can be anything"
} "none"

CCTK_REAL gamma_th "Thermal gamma"
{
  1:* :: "Positive"
} 1.33

CCTK_REAL rho_eos_max "The maximum value available for rho"
{
  0:* :: "Positive"
} 0.0048572

CCTK_REAL eps_eos_max "The maximum value available for eps"
{
  0:* :: "Positive"
} 51.0

CCTK_INT piecewise_n "Number of polytropic pieces for piecewise EoS"
{
  1:10 :: "Positive number between 1 and 10"
} 1

CCTK_REAL piecewise_k0 "First polytropic constant for piecewise EoS"
{
  0:* :: "Positive"
} 0.0

CCTK_REAL piecewise_gamma[10] "Adiabatic index for piecewise EoS"
{
  0:* :: "Positive"
} 0.0

CCTK_REAL piecewise_rho[9] "Density values at separation points for piecewise EoS"
{
  0:* :: "Positive"
} 0.0


######################################################
########### PARAMETERS FOR THE ATMOSPHERE ############
######################################################
### EoS ###
CCTK_REAL poly_k "Polytropic constant for poly EoS"
{
  0:* :: "Positive"
} 110.0

CCTK_REAL poly_gamma "Adiabatic index for poly EoS"
{
  0:* :: "Positive"
} 2.0

CCTK_REAL rho_atm "Floor value on the baryonic rest mass density (atmosphere)"
{
  0:* :: "Positive"
} 1.0e-11

CCTK_REAL atm_tolerance "A point is set to atmosphere in the Cons2Prim if q<q*(1+atm_tolerance), where 'q' is the quantity chosen with the Atmosphere_check parameter." STEERABLE=ALWAYS
{
  0:* :: "Positive"
} 0.0

### Utils ###
KEYWORD Atmosphere_check "Which quantity to use to set the atmosphere."
{
  "rho"   :: "density"
  "press" :: "pressure"
  "both"  :: "both"
} "rho"

CCTK_REAL rho_cut "The minimum rho below which evolution is turned off (atmosphere)." STEERABLE=recover
{
  0:* :: "Positive"
} 1.0e-8

### Magnetosphere ###
KEYWORD Atmosphere_method "How to manage the electromagnetic field in the atmosphere"
{
  "none"  :: "Evaluate the magnetosphere with the Evec_evolution_method"
  "fromS" :: "Evaluate the electric field from S"
  "zeroB" :: "Set the magnetic field equal to zero in the atmosphere; electric field is computed as with the 'none'"
} "none"


######################################################
############ PARAMETERS FOR THE OHM'S LAW ############
######################################################
KEYWORD Ohm_method "Method for the Ohm's law"
{
  "none"             :: "Ohm's law is not used; set 'none' if Evec_evolution_method is 'IMHD'"
  "Isotropic"        :: "Isotropic Ohm's law; Ideal MHD is Isotropic with eta_ohm0=0"
} "none"


CCTK_REAL eta_ohm0 "Value of eta0=1/sigma0"
{
  0:* :: "Positive"
} 0.0


######################################################
############## PARAMETERS FOR ANALYSIS ###############
######################################################
BOOLEAN perform_analysis "perform analysis?"
{
} "yes"

BOOLEAN compute_EMenergy "compute the electrocmagnetic energy density?"
{
} "yes"

BOOLEAN compute_norms "compute vector norms?"
{
} "yes"


######################################################
################# USEFUL PARAMETERS ##################
######################################################
### BOOLEAN ###
BOOLEAN update_Tmunu "Update Tmunu, for RHS of Einstein's equations?" STEERABLE=ALWAYS
{
} "yes"

### KEYWORD ###
KEYWORD c2p_fails_method "What should I do if the c2p algorithm fails?"
{
  "none"       :: "do nothing"
  "abort"      :: "stop the simulation"
  "alert"      :: "print a warning message and continue the simulation"
  "atmosphere" :: "set the atmosphere"
} "none"

### INTEGER ###
CCTK_INT c2p_counter_max "Maximum number of iterations for Cons2Prim solve"
{
  1:* :: "Positive"
} 1000

CCTK_INT derivative_order "" STEERABLE=RECOVER
{
 2 :: "second order"
 4 :: "fourth order"
} 4

CCTK_INT MIR_stencil "Width of the stencil"
{
  0:* :: "Positive"
} 3

### REAL ###
CCTK_REAL c2p_tolerance "Tolerance for primitive variable solve"
{
  0:* :: "Positive"
} 1.0e-10

CCTK_REAL magnetic_zero "The value below which the magnetic field is set equal to zero. NOTE: It is also used for the electric field"
{
  0:* :: "Positive"
} 1.0d-20

CCTK_REAL maximum_lorentz_factor "Maximum value for the Lorentz factor"
{
 1:* :: "Positive"
} 2000.0




