/****************************************
 ********* HEADER FOR THORN MIR *********
 ****************************************/
#ifndef MIR_HEADER_H
#define MIR_HEADER_H


/* ----------- CCTK HEADERS ------------- */
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Functions.h"

#define MIR_WARN(a,b) CCTK_WARN(a,adjustl(trim(b)))

/* --------- USEFUL DEFINITIONS ------------ */
#define Atmosphere_method_none   0
#define Atmosphere_method_fromS  1
#define Atmosphere_method_zeroB  2

#define Echarge_method_none 0
#define Echarge_method_fd   1
#define Echarge_method_flux 2

#define EoS_type_idealfluid 1
#define EoS_type_piecewise  2
#define EoS_type_taub       3
#define EoS_type_tabulated  4

#define Evec_method_none       0
#define Evec_method_kin_fromS  1
#define Evec_method_imhd       2
#define Evec_method_ohm        3

#define Ohm_method_none 0
#define Ohm_method_iso  1

#define eta_method_none  0
#define eta_method_const 1


#endif
