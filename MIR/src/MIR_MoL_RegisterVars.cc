// ===============================================================
// MoL_RegisterVars for thorn MIR                            
// ===============================================================

#include "MIR.h"


//===============================================================
// FUNCTION MIR_MoL_RegisterVars_hydro
// PURPOSE  register hydro variables, metric terms and stress-energy tensor for thorn MoL
//===============================================================
extern "C" void MIR_MoL_RegisterVars_hydro(CCTK_ARGUMENTS){

   DECLARE_CCTK_ARGUMENTS;
   DECLARE_CCTK_PARAMETERS;

   CCTK_INT ierr = 0, group, rhs;

   /* Constrained quantities */
   ierr += MoLRegisterConstrainedGroup(CCTK_GroupIndex("HydroBase::rho"));
   ierr += MoLRegisterConstrainedGroup(CCTK_GroupIndex("HydroBase::press"));
   ierr += MoLRegisterConstrainedGroup(CCTK_GroupIndex("HydroBase::eps"));
   ierr += MoLRegisterConstrainedGroup(CCTK_GroupIndex("HydroBase::vel"));
   ierr += MoLRegisterConstrainedGroup(CCTK_GroupIndex("HydroBase::w_lorentz"));

   if (*stress_energy_state) {
      ierr += MoLRegisterConstrainedGroup(CCTK_GroupIndex("TmunuBase::stress_energy_scalar"));
      ierr += MoLRegisterConstrainedGroup(CCTK_GroupIndex("TmunuBase::stress_energy_vector"));
      ierr += MoLRegisterConstrainedGroup(CCTK_GroupIndex("TmunuBase::stress_energy_tensor"));
   }

   /* Registration of grid functions */
   //--- Density ---
   if (CCTK_EQUALS(evolution_method,"MIR")) {
      group = CCTK_GroupIndex("MIR::rho_cons");
      rhs = CCTK_GroupIndex("MIR::rho_rhs");
      ierr += MoLRegisterEvolvedGroup(group,rhs);
   } //if Density

   //--- Internal energy ---
   if (CCTK_EQUALS(evolution_method,"MIR")) {
      group = CCTK_GroupIndex("MIR::en_cons");
      rhs = CCTK_GroupIndex("MIR::en_rhs");
      ierr += MoLRegisterEvolvedGroup(group,rhs);
   } //if Internal energy
   
   //--- Momentum ---
   if (CCTK_EQUALS(evolution_method,"MIR") || CCTK_EQUALS(Evec_evolution_method,"kin_fromS") || CCTK_EQUALS(Atmosphere_method,"fromS")) {
      group = CCTK_GroupIndex("MIR::Scons");
      rhs = CCTK_GroupIndex("MIR::S_rhs");
      ierr += MoLRegisterEvolvedGroup(group,rhs);
   } //if Momentum
   
   if (ierr) CCTK_ERROR("Problems registering with MoL (hydro)");

   /* Registration of ADMBase variables */
   ierr += MoLRegisterSaveAndRestoreGroup(CCTK_GroupIndex("ADMBase::lapse"));
   ierr += MoLRegisterSaveAndRestoreGroup(CCTK_GroupIndex("ADMBase::shift"));
   ierr += MoLRegisterSaveAndRestoreGroup(CCTK_GroupIndex("ADMBase::metric"));
   ierr += MoLRegisterSaveAndRestoreGroup(CCTK_GroupIndex("ADMBase::curv"));

   if (ierr) CCTK_ERROR("Problems registering with MoLRegisterSaveAndRestoreGroup");

} //end function MIR_MoL_RegisterVars_hydro

//===============================================================
// FUNCTION MIR_MoL_RegisterVars_em
// PURPOSE  register electromagnetic variables for thorn MoL
//===============================================================
extern "C" void MIR_MoL_RegisterVars_em(CCTK_ARGUMENTS){

   DECLARE_CCTK_ARGUMENTS;
   DECLARE_CCTK_PARAMETERS;

   CCTK_INT ierr = 0, group, rhs;

   /* Constrained quantities */
   ierr += MoLRegisterConstrainedGroup(CCTK_GroupIndex("HydroBase::Bvec"));
   ierr += MoLRegisterConstrainedGroup(CCTK_GroupIndex("ElectroBase::Evec"));
   if (!CCTK_EQUALS(Echarge_evolution_method,"none")) {
      ierr += MoLRegisterConstrainedGroup(CCTK_GroupIndex("ElectroBase::Echarge"));
   }
   if (CCTK_EQUALS(Evec_evolution_method,"Ohm")) {
      ierr += MoLRegisterConstrainedGroup(CCTK_GroupIndex("ElectroBase::eta_ohm"));
   }

   /* Registration of grid functions */
   //--- Magnetic field ---
   if (CCTK_EQUALS(Bvec_evolution_method,"MIR")) {
      group = CCTK_GroupIndex("MIR::Bcons");
      rhs = CCTK_GroupIndex("MIR::B_rhs");
      ierr += MoLRegisterEvolvedGroup(group,rhs);
   } //if Magnetic field
   
   //--- Electric field ---
   if (CCTK_EQUALS(Evec_evolution_method,"Ohm")) {
      group = CCTK_GroupIndex("MIR::Econs");
      rhs = CCTK_GroupIndex("MIR::E_rhs");
      ierr += MoLRegisterEvolvedGroup(group,rhs);
   } else if (CCTK_EQUALS(Echarge_evolution_method,"finite_differences")) {
      ierr += MoLRegisterConstrainedGroup(CCTK_GroupIndex("MIR::Econs"));
   }//if Electric field
   
   if (ierr) CCTK_ERROR("Problems registering with MoL (mhd)");

} //end function MIR_MoL_RegisterVars_em





