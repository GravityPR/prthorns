! ===============================================================
! c2p for thorn MIR
! ===============================================================

#include "MIR.h"


!==============================================================
! ROUTINE c2p_atm_em
! PORPOSE evaluate electric and magnetic field in the atmosphere
!==============================================================
subroutine c2p_atm_em(Bvec,Evec,Scons,g)

   use mod_common,only: do_atm_method
   use mod_metric,only: metric

   implicit none

   DECLARE_CCTK_PARAMETERS

   type(metric),intent(in) :: g
   CCTK_REAL,dimension(3),intent(in) :: Scons
   CCTK_REAL,dimension(3),intent(inout) :: Bvec
   CCTK_REAL,dimension(3),intent(out) :: Evec

   CCTK_REAL :: B2

   ! Magnetic field
   call check_Bvec(Bvec,B2,g,.true.)

   ! Electric field
   Evec=0.0d0 !IMHD

   !!! from S
   if (do_atm_method.eq.Atmosphere_method_fromS) then
      call c2p_ElectricField_fromS(Evec,Bvec,Scons,g)
      call check_Evec(Evec,B2,g)
   endif

end subroutine c2p_atm_em

!==============================================================
! ROUTINE c2p_ElectricField_fromS
! PORPOSE evaluate the electric field inverting the expression for the conserved momentum S_i
! NOTE    here we force E\cdot B=0
!==============================================================
subroutine c2p_ElectricField_fromS(E,B,S,g)
   
   use mod_common,only: Wmax
   use mod_math,only: math_cross_product
   use mod_metric

   implicit none

   type(metric),intent(in) :: g
   CCTK_REAL,dimension(3),intent(in) :: B,S
   CCTK_REAL,dimension(3),intent(out) :: E

   CCTK_REAL,dimension(3) :: B_cov,S_cov
   CCTK_REAL :: sdetg,B2,S2,f

   ! Check on B2
   B2=metric_norm2(g,B)
   if (B2.eq.0.0d0) then
      E=0.0d0
      return
   endif

   ! Check S2
   B_cov=metric_con2cov(g,B)
   S_cov=S-dot_product(S,B)*B_cov/B2
   S2=metric_norm2(g,S_cov,cov=.true.)
   if (S2.eq.0.0d0) then
      E=0.0d0
      return
   endif

   ! Value from S
   sdetg=g%sdet
   f=B2*sqrt((1.0d0-1.0d0/(Wmax**2))/S2)
   S_cov=S_cov*min(1.0d0,f)
   E=math_cross_product(B_cov,S_cov)/sdetg/B2

end subroutine c2p_ElectricField_fromS

!==============================================================
! ROUTINE c2p_ElectricField_OHM
! PORPOSE the implicit step for the electric field using the Ohm's law
!==============================================================
subroutine c2p_ElectricField_OHM(E,v,W,B,Estar,eta,g)

   use mod_math,only: math_cross_product
   use mod_metric

   implicit none
   
   type(metric),intent(in) :: g
   CCTK_REAL,dimension(3),intent(in) :: v,B,Estar
   CCTK_REAL,intent(in) :: W,eta
   CCTK_REAL,dimension(3),intent(out) :: E

   CCTK_REAL :: sdetg,vE
   CCTK_REAL,dimension(3) :: v_cp_B,v_cov
   CCTK_REAL,dimension(0:2) :: A

   ! Useful quantities
   sdetg=g%sdet
   v_cp_B=math_cross_product(v,B)*sdetg
   v_cp_B=metric_cov2con(g,v_cp_B)
   v_cov=metric_con2cov(g,v)
   vE=dot_product(v_cov,Estar)

   ! Coefficients
   call c2p_ElectricField_OHM_coefficients(A,W,eta)

   ! Electric field
   E=A(0)*Estar-A(1)*v_cp_B+A(2)*vE*v

end subroutine c2p_ElectricField_OHM

!==============================================================
! ROUTINE c2p_ElectricField_OHM_coefficients
! PORPOSE evaluate coefficients A_i of the implicit step for the
!         electric field using the Ohm's law
!==============================================================
subroutine c2p_ElectricField_OHM_coefficients(Acoeff,W,eta)

   implicit none
   
   CCTK_REAL,intent(in) :: W,eta
   CCTK_REAL,dimension(0:2),intent(out) :: Acoeff

   CCTK_REAL :: A0,A1,A2
   CCTK_REAL :: dA0,dA1
   CCTK_REAL :: y,z

   ! Useful quantities
   y=eta/W
   z=eta*W

   ! Coefficients A_i
   A1=1.0d0/(y+1.0d0)
   A0=1.0d0-A1
   A2=1.0d0-(1.0d0/(z+1.0d0))
   A2=A1*A2

   Acoeff(0)=A0
   Acoeff(1)=A1
   Acoeff(2)=A2

end subroutine c2p_ElectricField_OHM_coefficients

!==============================================================
! ROUTINE c2p_hydro
! PORPOSE derive density, internal energy and pressure from the conserved variables
! NOTE here 'en' is the hydro conserved energy
!==============================================================
subroutine c2p_hydro(rho,eps,press,D,M,en,W)

   use mod_eos,only: rho_max,rho_min
   use mod_eos,only: eps_max,eps_min

   implicit none
   
   CCTK_REAL,intent(in) :: D,M,en,W
   CCTK_REAL,intent(out) :: rho,eps,press

   CCTK_REAL :: u,q,r

   u=sqrt(W**2-1.0d0)
   q=en/D
   r=M/D

   ! Density
   rho=D/W
   rho=max(min(rho_max,D/W),rho_min)

   ! Internal energy
   eps=W*q-r*u-1.0d0
   eps=max(min(eps_max,eps),eps_min)

   ! Pressure
   call eos_press(press,rho,eps)

end subroutine c2p_hydro

!==============================================================
! ROUTINE c2p_vel
! PORPOSE evaluate the maximum value for the fluid momentum
!==============================================================
subroutine c2p_Mmax(Mmax,S_cov,Bvec,Estar,eta,g)
   
   use mod_common,only: do_Evec_method
   use mod_metric,only: metric,metric_norm2
   implicit none

   type(metric),intent(in) :: g
   CCTK_REAL,intent(in) :: eta
   CCTK_REAL,dimension(3),intent(in) :: S_cov,Bvec,Estar
   CCTK_REAL,intent(out) :: Mmax

   CCTK_REAL :: A0
   CCTK_REAL :: delta
   CCTK_REAL :: B2,E2,S2

   ! hydro and IMHD case
   if (do_Evec_method.ne.Evec_method_ohm) then
      S2=metric_norm2(g,S_cov,cov=.true.)
      Mmax=sqrt(S2)
      return
   endif

   ! norms
   E2=metric_norm2(g,Estar)
   B2=metric_norm2(g,Bvec)
   S2=metric_norm2(g,S_cov,cov=.true.)

   ! Coefficients
   A0=1.0d0-1.0d0/(1.0d0+eta)

   ! evaluate Mmax
   delta=A0*A0*E2*B2+S2
   Mmax=A0*sqrt(E2*B2)
   Mmax=Mmax+sqrt(delta)

end subroutine c2p_Mmax

!==============================================================
! ROUTINE c2p_vel
! PORPOSE derive the fluid velocity from the conserved variables
!==============================================================
subroutine c2p_vel(vel,rho,eps,press,W,M_cov,vmax,g)
   
   use mod_metric
   implicit none

   type(metric),intent(in) :: g
   CCTK_REAL,intent(in) :: rho,eps,press,W,vmax
   CCTK_REAL,dimension(3),intent(in) :: M_cov
   CCTK_REAL,dimension(3),intent(out) :: vel

   CCTK_REAL :: rhohW2,W2,v2,v2max
   CCTK_REAL,dimension(3) :: M_con

   ! Evaluate vel
   W2=W*W
   rhohW2=(rho*(1.0d0+eps)+press)*W2
   M_con=metric_cov2con(g,M_cov)
   vel=M_con/rhohW2

   ! Evaluate norm
   v2=metric_norm2(g,vel)
   v2max=vmax*vmax

   ! Renormalize
   if (v2.gt.v2max) then
      vel=vel*sqrt(v2max/v2)
   endif

end subroutine c2p_vel




