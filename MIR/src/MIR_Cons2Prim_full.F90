! ===============================================================
! Cons2Prim for thorn MIR
! ===============================================================

#include "MIR.h"


!==============================================================
! ROUTINE MIR_Cons2Prim_full
! PORPOSE from conservative to primitive variables in the full (magneto)hydroninamic case
!==============================================================
subroutine MIR_Cons2Prim_full(CCTK_ARGUMENTS)

   use mod_common
   use mod_math,only: math_cross_product
   use mod_metric
   
   implicit none

   DECLARE_CCTK_ARGUMENTS
   DECLARE_CCTK_PARAMETERS
   DECLARE_CCTK_FUNCTIONS
   
   ! Useful quantities
   CCTK_INT :: i,j,k
   CCTK_INT :: warnlevel

   !!! metric terms
   type(metric) :: g
   CCTK_REAL :: sdetg

   !!! IMEX terms
   CCTK_REAL :: dt,Aimex

   !!! Scalars
   CCTK_REAL :: D,en
   CCTK_REAL :: rholoc,epsloc,ploc,Wloc
   CCTK_REAL :: S2,B2,eta_loc
   
   !!! Vectors
   CCTK_REAL,dimension(3) :: S_cov,v_con
   CCTK_REAL,dimension(3) :: B_con,E_con,Estar

   !!! Logicals
   logical :: is_atm,is_fails,is_Estar

   !!! Characters
   character(len=250) :: warnline

   if(cctk_iteration.eq.0 .or. is_checkpoint) return
   
   ! Allocations
   call common_alloc(CCTK_PASS_FTOF)
   dt=CCTK_DELTA_TIME

   ! Warning level
   if (CCTK_EQUALS(c2p_fails_method,"abort")) then
      warnlevel=CCTK_WARN_ABORT
   else if (CCTK_EQUALS(c2p_fails_method,"alert")) then
      warnlevel=CCTK_WARN_ALERT
   else
      warnlevel=-1
   endif

   ! Loops
   !$OMP PARALLEL DO PRIVATE(k,j,i,g,sdetg,Aimex,&
   !$OMP D,en,S_cov,S2,Estar,eta_loc,&
   !$OMP rholoc,epsloc,ploc,Wloc,v_con,B_con,B2,E_con,&
   !$OMP is_atm,is_fails,is_Estar,warnline)
   do k=1,nz
   do j=1,ny
   do i=1,nx
      ! is_fails flag
      is_fails=.false.
      c2p_fails_mask(i,j,k)=0.0d0

      ! 3-metric
      call metric_alloc(g,gxx(i,j,k),gxy(i,j,k),gxz(i,j,k),gyy(i,j,k),gyz(i,j,k),gzz(i,j,k))
      sdetg=g%sdet

      ! Check for NaN
      if ((sdetg.ne.sdetg) .or. (alp(i,j,k).ne.alp(i,j,k)) .or. (betax(i,j,k).ne.betax(i,j,k)) .or.&
           (betay(i,j,k).ne.betay(i,j,k)) .or. (betaz(i,j,k).ne.betaz(i,j,k))) then
         !$OMP CRITICAL
         call MIR_WARN(1,"========================================================================")
         call MIR_WARN(1,"CONS2PRIM: STOPPING THE CODE")
         call MIR_WARN(1,"====== LOCATION POINTS ======")
         write(warnline,'(a20,3i5)')    "i j k = ",i,j,k
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,3g16.7)') "x y z = ",x(i,j,k),y(i,j,k),z(i,j,k)
         call MIR_WARN(1,warnline)
         call MIR_WARN(1,"====== METRIC TERMS ======")
         write(warnline,'(a20,g16.7)') "alp = ",alp(i,j,k)
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,3g16.7)') "betax betay betaz = ",betax(i,j,k),betay(i,j,k),betaz(i,j,k)
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,3g16.7)') "gxx gxy gxz = ",gxx(i,j,k),gxy(i,j,k),gxz(i,j,k)
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,3g16.7)') "gyy gyz gzz = ",gyy(i,j,k),gyz(i,j,k),gzz(i,j,k)
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,g16.7)')  "sdetg       = ",sdetg
         call MIR_WARN(CCTK_WARN_ABORT," - CASE A - NaN in metric terms")
         !$OMP END CRITICAL
      endif
      
      ! ------------------------------------------------------------------------ !
      ! Hydro conserved variables
      D=rho_cons(i,j,k)/sdetg
      en=en_cons(i,j,k)/sdetg
      S_cov=Scons(i,j,k,:)/sdetg

      ! Check for atmosphere
      call check_atm_rho(is_atm,D)

      ! Set atmosphere
      if (is_atm) then
         rho(i,j,k)=rho_atm
         call atm_eps(eps(i,j,k))
         call atm_press(press(i,j,k))
         call atm_vel(vel(i,j,k,:),w_lorentz(i,j,k))
         if (do_evo_Bvec) then
            B_con=Bcons(i,j,k,:)/sdetg
            call c2p_atm_em(B_con,E_con,S_cov,g) !the check for B_con and S_cov are performed here
            Bvec(i,j,k,:)=B_con
            Evec(i,j,k,:)=E_con
            if(do_Evec_method.eq.Evec_method_ohm) eta_ohm(i,j,k)=0.0d0
         endif
         cycle
      endif

      ! ------------------------------------------------------------------------ !
      ! Magnetic field
      B_con=0.0d0
      B2=0.0d0
      if (do_evo_Bvec) then
         B_con=Bcons(i,j,k,:)/sdetg
         call check_Bvec(B_con,B2,g,is_atm)
      endif

      ! Check for energy
      call check_en_cons(en,D,B2,is_atm)

      ! Check for S
      call check_Scons(S_cov,S2,en,B2,g)

      ! IMEX coefficient
      if (do_Evec_method.eq.Evec_method_ohm) then
         Aimex=alp(i,j,k)*RKImexCoefficient*dt
      else
         Aimex=0.0d0
      endif
      
      ! Electric field after MoL step and electric resistivity
      Estar=0.0d0
      eta_loc=0.0d0
      is_Estar=.false.
      if (do_Evec_method.eq.Evec_method_ohm) then
         Estar=Econs(i,j,k,:)/sdetg
         if (RKImexCoefficient.eq.0) then
            call check_Evec(Estar,B2,g)
            eta_loc=1.0d200 !this ensures that E=Estar from c2p_ElectrField_OHM function, although it shouldn't be necessary
            is_Estar=.true.
         else
            eta_loc=eta_ohm(i,j,k)
            eta_loc=eta_loc/Aimex
            !call check_Estar(Estar,B2,eta_loc,g)
         endif
      endif
      
      ! ------------------------------------------------------------------------ !
      ! Check for NaN
      if ((D.ne.D) .or. (en.ne.en) .or. (S_cov(1).ne.S_cov(1)) .or.&
          (S_cov(2).ne.S_cov(2)) .or. (S_cov(3).ne.S_cov(3))) then
         is_fails=.true.
      endif
      if (do_evo_Bvec) then
         if ((B_con(1).ne.B_con(1)) .or. (B_con(2).ne.B_con(2)) .or. (B_con(3).ne.B_con(3))) then
            is_fails=.true.
         endif
         if (do_Evec_method.eq.Evec_method_ohm) then
            if ((Estar(1).ne.Estar(1)) .or. (Estar(2).ne.Estar(2)) .or. (Estar(3).ne.Estar(3))) then
               is_fails=.true.
            endif
         endif
      endif
      if(is_fails) c2p_fails_mask(i,j,k)=1.0d0

      ! Set atmosphere if is_fails is true
      if (is_fails .and. CCTK_EQUALS(c2p_fails_method,"atmosphere")) then
         rho(i,j,k)=rho_atm
         call atm_eps(eps(i,j,k))
         call atm_press(press(i,j,k))
         call atm_vel(vel(i,j,k,:),w_lorentz(i,j,k))
         if (do_evo_Bvec) then
            call c2p_atm_em(B_con,E_con,S_cov,g)
            Bvec(i,j,k,:)=B_con
            Evec(i,j,k,:)=E_con
            if(do_Evec_method.eq.Evec_method_ohm) eta_ohm(i,j,k)=0.0d0
         endif
         cycle
      endif

      ! Print a message if is_fails is true and I want to stop the code
      if (is_fails .and. warnlevel.eq.CCTK_WARN_ABORT) then
         !$OMP CRITICAL
         call MIR_WARN(1,"========================================================================")
         call MIR_WARN(1,"CONS2PRIM: STOPPING THE CODE")
         call MIR_WARN(1,"====== LOCATION POINTS ======")
         write(warnline,'(a20,3i5)')    "i j k = ",i,j,k
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,3g16.7)') "x y z = ",x(i,j,k),y(i,j,k),z(i,j,k)
         call MIR_WARN(1,warnline)
         call MIR_WARN(1,"====== METRIC TERMS ======")
         write(warnline,'(a20,3g16.7)') "gxx gxy gxz = ",gxx(i,j,k),gxy(i,j,k),gxz(i,j,k)
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,3g16.7)') "gyy gyz gzz = ",gyy(i,j,k),gyz(i,j,k),gzz(i,j,k)
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,g16.7)')  "sdetg       = ",sdetg
         call MIR_WARN(1,warnline)
         call MIR_WARN(1,"====== CONSERVATIVE VARIABLES ======")
         write(warnline,'(a20,g16.7)')  "D           = ",D
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,g16.7)')  "en          = ",en
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,3g16.7)') "S_x S_y S_z = ",S_cov
         call MIR_WARN(1,warnline)
         if (do_evo_Bvec) then
            write(warnline,'(a20,3g16.7)') "B^x B^y B^z = ",B_con
            call MIR_WARN(1,warnline)
            if (do_Evec_method.eq.Evec_method_ohm) then
               write(warnline,'(a30,3g16.7)') "Estar^x Estar^y Estar^z = ",Estar
               call MIR_WARN(1,warnline)
               write(warnline,'(a20,2g16.7)') "eta eta/Aimex           = ",eta_ohm(i,j,k),eta_loc
               call MIR_WARN(1,warnline)
               write(warnline,'(a30,2g16.7)') "RKImexCoefficient Aimex = ",RKImexCoefficient,Aimex
               call MIR_WARN(1,warnline)
            endif
         endif
         call MIR_WARN(CCTK_WARN_ABORT," - CASE B - NaN in conserved variables")
         !$OMP END CRITICAL
      endif

      ! ------------------------------------------------------------------------ !
      ! Bisection
      call MIR_Cons2Prim_bisection(is_atm,is_fails,rholoc,epsloc,ploc,v_con,Wloc,B_con,E_con,&
                                   D,en,S_cov,Estar,eta_loc,is_Estar,g)

      ! Update mask for c2p failure
      if(is_fails) c2p_fails_mask(i,j,k)=1.0d0
      
      ! Set atmosphere
      if (is_atm .or. (is_fails .and. CCTK_EQUALS(c2p_fails_method,"atmosphere"))) then
         rho(i,j,k)=rho_atm
         call atm_eps(eps(i,j,k))
         call atm_press(press(i,j,k))
         call atm_vel(vel(i,j,k,:),w_lorentz(i,j,k))
         if (do_evo_Bvec) then
            call c2p_atm_em(B_con,E_con,S_cov,g)
            Bvec(i,j,k,:)=B_con
            Evec(i,j,k,:)=E_con
            if(do_Evec_method.eq.Evec_method_ohm) eta_ohm(i,j,k)=0.0d0
         endif
         cycle
      endif

      ! Print a message if bisection fails
      if (is_fails .and. warnlevel.ge.0) then
         !$OMP CRITICAL
         call MIR_WARN(1,"========================================================================")
         if (warnlevel.eq.CCTK_WARN_ABORT) then
            call MIR_WARN(1,"CONS2PRIM: BISECTION FAILED. STOPPING THE CODE.")
         else
            call MIR_WARN(1,"CONS2PRIM: BISECTION FAILED.")
         endif
         call MIR_WARN(1,"====== LOCATION POINTS ======")
         write(warnline,'(a20,3i5)')    "i j k = ",i,j,k
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,3g16.7)') "x y z = ",x(i,j,k),y(i,j,k),z(i,j,k)
         call MIR_WARN(1,warnline)
         call MIR_WARN(1,"====== METRIC TERMS ======")
         write(warnline,'(a20,3g16.7)') "gxx gxy gxz = ",gxx(i,j,k),gxy(i,j,k),gxz(i,j,k)
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,3g16.7)') "gyy gyz gzz = ",gyy(i,j,k),gyz(i,j,k),gzz(i,j,k)
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,g16.7)')  "sdetg       = ",sdetg
         call MIR_WARN(1,warnline)
         call MIR_WARN(1,"====== CONSERVATIVE VARIABLES ======")
         write(warnline,'(a20,g16.7)')  "D           = ",D
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,g16.7)')  "en          = ",en
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,3g16.7)') "S_x S_y S_z = ",S_cov
         call MIR_WARN(1,warnline)
         call MIR_WARN(1,"====== PRIMITIVE VARIABLES ======")
         write(warnline,'(a20,g16.7)')  "rho         = ",rholoc
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,g16.7)')  "press       = ",ploc
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,g16.7)')  "eps         = ",epsloc
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,3g16.7)') "v^x v^y v^z = ",v_con
         call MIR_WARN(1,warnline)
         write(warnline,'(a20,g16.7)')  "W           = ",Wloc
         call MIR_WARN(1,warnline)
         if (do_evo_Bvec) then
            write(warnline,'(a20,3g16.7)') "B^x B^y B^z = ",B_con
            call MIR_WARN(1,warnline)
            write(warnline,'(a20,3g16.7)') "E^x E^y E^z = ",E_con
            call MIR_WARN(1,warnline)
            if (do_Evec_method.eq.Evec_method_ohm) then
               write(warnline,'(a30,3g16.7)') "Estar^x Estar^y Estar^z = ",Estar
               call MIR_WARN(1,warnline)
               write(warnline,'(a20,L1)')     "E=Estar?                  ",is_Estar
               call MIR_WARN(1,warnline)
               write(warnline,'(a20,2g16.7)') "eta eta/Aimex           = ",eta_ohm(i,j,k),eta_loc
               call MIR_WARN(1,warnline)
               write(warnline,'(a30,2g16.7)') "RKImexCoefficient Aimex = ",RKImexCoefficient,Aimex
               call MIR_WARN(1,warnline)
            endif
         endif
         call MIR_WARN(warnlevel," - CASE B - NaN in primitive variables or bisection failure")
         !$OMP END CRITICAL
      endif
      
      ! Update matrices
      rho(i,j,k)=rholoc
      eps(i,j,k)=epsloc
      press(i,j,k)=ploc
      vel(i,j,k,:)=v_con
      w_lorentz(i,j,k)=Wloc
      if (do_evo_Bvec) then
         Bvec(i,j,k,:)=B_con
         Evec(i,j,k,:)=E_con
      endif
   enddo
   enddo
   enddo
   !$OMP END PARALLEL DO

end subroutine MIR_Cons2Prim_full

!==============================================================
! ROUTINE MIR_Cons2Prim_bisection
! PORPOSE performe the bisection to derive primitive variables
!==============================================================
subroutine MIR_Cons2Prim_bisection(is_atm,is_fails,rho,eps,press,vel,W,Bvec,Evec,&
                                   D,en,S_cov,Estar,eta,is_Estar,g)

   use mod_common,only: do_evo_Bvec,do_Evec_method
   use mod_common,only: Wmax
   use mod_eos
   use mod_math,only: math_cross_product
   use mod_metric
   
   implicit none

   DECLARE_CCTK_PARAMETERS
   DECLARE_CCTK_FUNCTIONS
   
   type(metric),intent(in) :: g
   CCTK_REAL,intent(in) :: D,en
   CCTK_REAL,dimension(3),intent(in) :: Estar,S_cov
   logical,intent(in) :: is_Estar

   CCTK_REAL,intent(in) :: eta
   CCTK_REAL,dimension(3),intent(inout) :: Bvec
   
   CCTK_REAL,intent(out) :: rho,eps,press,W
   CCTK_REAL,dimension(3),intent(out) :: Evec,vel
   logical,intent(out) :: is_atm,is_fails

   CCTK_INT :: counter
   CCTK_REAL :: mu_minus,mu_plus,mu
   CCTK_REAL :: floc,f_minus

   CCTK_REAL :: sdetg
   CCTK_REAL :: en_fluid,B2,Uem
   CCTK_REAL :: M,M2,Mmax
   CCTK_REAL :: v,vmax
   CCTK_REAL :: h,r
   CCTK_REAL,dimension(3) :: M_cov,EcpB
   logical :: is_zero

   ! Metric determinant
   sdetg=g%sdet

   ! Maximum value for the fluid momentum
   call c2p_Mmax(Mmax,S_cov,Bvec,Estar,eta,g)

   ! Initial bounds
   mu_minus=0.0d0
   mu_plus=1.0d0/enthalpy_min

   ! Initial f_minus
   ! in this case it is not important the value but only the negative sign
   f_minus=-1.0d0

   ! Maximum value for v
   r=Mmax/D
   vmax=r/enthalpy_min
   vmax=vmax/sqrt(1.0d0+(vmax**2))
   vmax=min(vmax,sqrt(1.0d0-1.0d0/(Wmax**2)))

   ! Conserved variables in the hydro case
   ! checks have already been performed in the main routine
   if (.not.do_evo_Bvec) then
      en_fluid=en
      M_cov=S_cov
      M2=metric_norm2(g,M_cov,cov=.true.)
      M=sqrt(M2)
      r=M/D
   endif

   ! Conserved hydro variables in the resistive case if Evec=Estar
   ! check for the electric field has already been performed in the main routine
   if (do_evo_Bvec .and. is_Estar) then
      Evec=Estar
      EcpB=math_cross_product(Evec,Bvec)*sdetg
      call em_EnergyDensity(Uem,Bvec,Evec,g)
      en_fluid=en-Uem
      call check_en_cons(en_fluid,D,0.0d0,.false.)
      M_cov=S_cov-EcpB
      call check_Scons(M_cov,M2,en_fluid,0.0d0,g)
      M=sqrt(M2)
      if (M.gt.Mmax) then
         M_cov=M_cov*sqrt(Mmax/M)
         M=Mmax
      endif
      r=M/D
   endif

   ! Bisection
   do counter=1,c2p_counter_max
      !!! Update mu
      mu=0.5d0*(mu_plus+mu_minus)

      !!! Derive Evec and conserved hydro variables
      if (do_evo_Bvec .and. .not.is_Estar) then
         call MIR_Cons2Prim_derive_Evec(Evec,mu,vmax,D,S_cov,Bvec,Estar,eta,g)
         EcpB=math_cross_product(Evec,Bvec)*sdetg
         call em_EnergyDensity(Uem,Bvec,Evec,g)
         en_fluid=en-Uem
         call check_en_cons(en_fluid,D,0.0d0,.false.)
         M_cov=S_cov-EcpB
         call check_Scons(M_cov,M2,en_fluid,0.0d0,g)
         M=sqrt(M2)
         if (M.gt.Mmax) then
            M_cov=M_cov*sqrt(Mmax/M)
            M=Mmax
         endif
         r=M/D
      endif

      !!! Derive v and W
      v=min(mu*r,vmax)
      W=1.0d0/sqrt(1.0d0-v**2)

      !!!! Derive hydro primitive variables
      call c2p_hydro(rho,eps,press,D,M,en_fluid,W)

      !!! Derive the new enthalpy
      h=1.0d0+eps+(press/rho)

      !!! Check for the master function
      floc=(h/W)+mu*(r**2)
      floc=mu-(1.0d0/floc)
      call check_zero(is_zero,floc,c2p_tolerance,counter,c2p_counter_max)
      if(is_zero) exit

      !!! Update bounds
      if ((floc*f_minus).lt.0.0d0) then
         mu_plus=mu
      else
         mu_minus=mu
         f_minus=floc
      endif
   enddo

   ! Check for bisection failure
   is_fails=.false.
   if(.not.is_zero) is_fails=.true.

   ! Check for NaN
   if ((rho.ne.rho) .or. (eps.ne.eps) .or. (press.ne.press) .or. (W.ne.W)) then
      is_fails=.true.
   endif

   if (is_fails .and. CCTK_EQUALS(c2p_fails_method,"atmosphere")) then
      return
   endif
      
   ! Check for atmosphere
   if (CCTK_EQUALS(Atmosphere_check,"rho") .or. CCTK_EQUALS(Atmosphere_check,"both")) then
      call check_atm_rho(is_atm,rho)
      if(is_atm) return
   endif

   if (CCTK_EQUALS(Atmosphere_check,"press") .or. CCTK_EQUALS(Atmosphere_check,"both")) then
      call check_atm_press(is_atm,press)
      if(is_atm) return
   endif
   
   ! Derive the fluid velocity
   call c2p_vel(vel,rho,eps,press,W,M_cov,vmax,g)

   ! Check for NaN
   if ((vel(1).ne.vel(1)) .or. (vel(2).ne.vel(2)) .or. (vel(3).ne.vel(3))) then
      is_fails=.true.
   endif

   if (is_fails .and. CCTK_EQUALS(c2p_fails_method,"atmosphere")) then
      return
   endif

   ! Derive the electric field again, to ensure coherence with the fluid velocity
   if (do_evo_Bvec .and. .not.is_Estar) then
      if (do_Evec_method.eq.Evec_method_imhd) then
         call em_ElectricField_IMHD(Evec,vel,Bvec,g)
      else
         call c2p_ElectricField_OHM(Evec,vel,W,Bvec,Estar,eta,g)
      endif
      B2=metric_norm2(g,Bvec)
      call check_Evec(Evec,B2,g)

      !!! Check for NaN
      if ((Evec(1).ne.Evec(1)) .or. (Evec(2).ne.Evec(2)) .or. (Evec(3).ne.Evec(3))) then
         is_fails=.true.
      endif
   endif

end subroutine MIR_Cons2Prim_bisection

!==============================================================
! ROUTINE MIR_Cons2Prim_derive_Evec
! PORPOSE derive the electric field during the bisection
!==============================================================
subroutine MIR_Cons2Prim_derive_Evec(Evec,mu,vmax,D,S_cov,Bvec,Estar,eta,g)

   use mod_common,only: do_Evec_method
   use mod_math
   use mod_metric
   
   implicit none

   DECLARE_CCTK_PARAMETERS
   DECLARE_CCTK_FUNCTIONS
   
   type(metric),intent(in) :: g
   CCTK_REAL,intent(in) :: D,eta,mu,vmax
   CCTK_REAL,dimension(3),intent(in) :: Bvec,Estar,S_cov
   CCTK_REAL,dimension(3),intent(out) :: Evec

   CCTK_INT :: counter,i,j,k
   CCTK_REAL :: sdetg,g_ij,eps_ijk
   CCTK_REAL :: Bvec2,b2
   CCTK_REAL :: re,x,Jloc,dA1
   CCTK_REAL :: v2,v2max,W
   CCTK_REAL,dimension(3) :: b_con,b_cov
   CCTK_REAL,dimension(3) :: e_con,e_cov,e_cross_b
   CCTK_REAL,dimension(3) :: estar_con,estar_cov
   CCTK_REAL,dimension(3) :: r_cov,r_cross_b
   CCTK_REAL,dimension(3) :: v_cov,v_con,v_cross_b
   CCTK_REAL,dimension(3) :: dW_by_de,de,floc
   CCTK_REAL,dimension(3,3) :: dvcov_by_de
   CCTK_REAL,dimension(3,3) :: Jac,invJac
   CCTK_REAL,dimension(0:2) :: Acoeff
   logical :: is_zero

   ! Utils
   sdetg=g%sdet
   b_con=Bvec/sqrt(D)
   b_cov=metric_con2cov(g,b_con)
   r_cov=S_cov/D
   Bvec2=metric_norm2(g,Bvec)
   b2=Bvec2/D

   ! IMHD case
   if (do_Evec_method.eq.Evec_method_imhd) then
      x=1.0d0+mu*b2           !1+mu*b^2
      x=1.0d0/x               !x=1/(1+mu*b^2)
      r_cross_b=math_cross_product(r_cov,b_cov)/sdetg
      e_con=-mu*x*r_cross_b
      Evec=sqrt(D)*e_con
      call check_Evec(Evec,Bvec2,g)
      return
   endif

   ! Useful quaintities
   v2max=vmax*vmax
   estar_con=Estar/sqrt(D)
   estar_cov=metric_con2cov(g,estar_con)
   do i=1,3
   do j=1,3
      eps_ijk=0.0d0
      do k=1,3
         eps_ijk=eps_ijk+math_LeviCivita(i,j,k)*b_con(k)
      enddo
      dvcov_by_de(i,j)=eps_ijk
   enddo
   enddo
   dvcov_by_de=-mu*dvcov_by_de

   ! Initial guess
   e_con=0.0d0
   e_cov=0.0d0

   ! Newton-Raphson
   do counter=1,c2p_counter_max
      !!! fluid velocity
      e_cross_b=math_cross_product(e_con,b_con)*sdetg
      v_cov=mu*(r_cov-e_cross_b)
      v_con=metric_cov2con(g,v_cov)
      v2=metric_norm2(g,v_con)
      if (v2.gt.v2max) then
         v_con=v_con*sqrt(v2max/v2)
         v_cov=v_cov*sqrt(v2max/v2)
      endif

      !!! Lorentz factor
      call hydro_W_from_v(W,v_con,g)

      !!! local function
      call c2p_ElectricField_OHM_coefficients(Acoeff,W,eta)
      re=dot_product(r_cov,e_con)
      v_cross_b=math_cross_product(v_con,b_con)*sdetg
      floc=Acoeff(0)*estar_cov-Acoeff(1)*v_cross_b+mu*Acoeff(1)*re*v_cov-e_cov
      call check_zero_vector(is_zero,floc,c2p_tolerance,counter,c2p_counter_max)
      if(is_zero) exit

      !!! Useful quantities
      !!! written this way to handle infinitives well
      dA1=Acoeff(0)*Acoeff(1)*W*W !this is (dA1/dW)*W^3
      dW_by_de=mu*v_cross_b       !this is (dW/de^j)/W^3

      !!! Jacobian matrix
      do i=1,3
      do j=1,3
         g_ij=g%cov(i,j)
         Jloc=mu*re*v_cov(i)-estar_cov(i)-v_cross_b(i)
         Jac(i,j)=Jloc*dW_by_de(j)*dA1
         Jloc=v_cov(i)*r_cov(j)+re*dvcov_by_de(i,j)
         Jloc=Jloc+b_cov(i)*b_cov(j)-b2*g_ij
         Jac(i,j)=Jac(i,j)+mu*Jloc*Acoeff(1)
         Jac(i,j)=Jac(i,j)-g_ij
      enddo
      enddo

      !!! inverse Jacobian matrix
      invJac=math_matrix_inverse(Jac,3)

      !!! J^{ij}f_i
      do i=1,3
         de(i)=dot_product(invJac(i,:),floc)
      enddo
      call check_zero_vector(is_zero,de,c2p_tolerance,counter,c2p_counter_max)
      if(is_zero) exit

      !!! new electric field
      e_con=e_con-de
      e_cov=metric_con2cov(g,e_con)
   enddo

   Evec=sqrt(D)*e_con
   call check_Evec(Evec,B2,g)

end subroutine MIR_Cons2Prim_derive_Evec







