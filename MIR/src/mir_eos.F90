! ===============================================================
! eos for thorn MIR                            
! ===============================================================

#include "MIR.h"


!==============================================================
! ROUTINE eos_alloc
! PORPOSE allocate quantities needed for the choosen EoS
! NOTE    this routine must be called at the beginning of the simulation and after the checkpoint
!==============================================================
subroutine eos_alloc()

   use mod_eos
   implicit none

   DECLARE_CCTK_PARAMETERS
   DECLARE_CCTK_FUNCTIONS

   ! Force Taub inequality
   do_force_taub=force_taub_inequality

   ! Ideal Fluid
   if (CCTK_EQUALS(eos_type,"Ideal_Fluid")) then
      do_eos_type=EoS_type_idealfluid
      call eos_idealfluid_limits(rho_min,rho_max,eps_min,eps_max,enthalpy_min)
      return
   endif

   ! Piecewise Polytropic
   if (CCTK_EQUALS(eos_type,"Piecewise")) then
      do_eos_type=EoS_type_piecewise
      call eos_piecewise_limits(rho_min,rho_max,eps_min,eps_max,enthalpy_min)
      return
   endif

   ! Taub
   if (CCTK_EQUALS(eos_type,"Taub")) then
      do_eos_type=EoS_type_taub
      call eos_taub_limits(rho_min,rho_max,eps_min,eps_max,enthalpy_min)
      return
   endif

   ! Tabulated
   if (CCTK_EQUALS(eos_type,"Tabulated")) then
      do_eos_type=EoS_type_tabulated
      call CCTK_ERROR("ERROR Tabulated EoS not yet implemented")
      return
   endif

   ! Error
   call CCTK_ERROR("ERROR EoS type")

end subroutine eos_alloc

!==============================================================
! ROUTINE eos_paramcheck
! PORPOSE call routines for paramcheck for the choosen EoS
! NOTE    this routine mis called only by MIR_ParamCheck
!==============================================================
subroutine eos_paramcheck()

   use mod_eos,only: do_eos_type
   implicit none

   DECLARE_CCTK_PARAMETERS
   DECLARE_CCTK_FUNCTIONS

   ! Ideal Fluid
   if (CCTK_EQUALS(eos_type,"Ideal_Fluid")) then
      call eos_idealfluid_paramcheck()
      return
   endif

   ! Piecewise Polytropic
   if (CCTK_EQUALS(eos_type,"Piecewise")) then
      call eos_piecewise_paramcheck()
      return
   endif

end subroutine eos_paramcheck

!==============================================================
! ROUTINE eos_cs2
! PORPOSE evaluate the square of the speed of sound for the choosen EoS
!==============================================================
subroutine eos_cs2(cs2,rho,eps,press)

   use mod_eos,only: do_eos_type
   implicit none
   
   CCTK_REAL,intent(out) :: cs2
   CCTK_REAL,intent(in) :: rho,eps,press

   ! Ideal Fluid
   if (do_eos_type.eq.EoS_type_idealfluid) then
      call eos_idealfluid_cs2(cs2,eps)
      return
   endif

   ! Piecewise Polytropic
   if (do_eos_type.eq.EoS_type_piecewise) then
      call eos_piecewise_cs2(cs2,rho,eps)
      return
   endif

   ! Taub
   if (do_eos_type.eq.EoS_type_taub) then
      call eos_taub_cs2(cs2,rho,press)
      return
   endif

end subroutine eos_cs2

!==============================================================
! ROUTINE eos_press
! PORPOSE derive the pressure for the choosen EoS
!==============================================================
subroutine eos_press(press,rho,eps)

   use mod_eos,only: do_eos_type
   implicit none
   
   CCTK_REAL,intent(out) :: press
   CCTK_REAL,intent(in) :: rho,eps

   ! Ideal Fluid
   if (do_eos_type.eq.EoS_type_idealfluid) then
      call eos_idealfluid_press(press,rho,eps)
      return
   endif  
   
   ! Piecewise Polytropic
   if (do_eos_type.eq.EoS_type_piecewise) then
      call eos_piecewise_press(press,rho,eps)
      return
   endif

   ! Taub
   if (do_eos_type.eq.EoS_type_taub) then
      call eos_taub_press(press,rho,eps)
      return
   endif

end subroutine eos_press




