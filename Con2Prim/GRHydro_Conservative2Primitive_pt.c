

#include "cctk.h"
#include "cctk_Parameters.h"
#include "GRHydro_Con2Prim.h"


/************************************************************************************

New local con2prim routine based on Siegel et al. arXiv:1712.07538v

*************************************************************************************/



/* main routine: 

This will be called directly by GRHydro_Con2PrimM.F90 (Fortran!!). All it does is some initial bookkeeping, call the main Newton-Raphson function and try to handle errors that may arise.

*/

// For now we just define these as macros, Dunno how this should actually be done!
#define NUM_PRIM = 12;
#define NUM_CON = 12;

void CCTK_FCALL CCTK_FNAME(GRHydro_Con2PrimM_pt) (
						  CCTK_INT *handle, CCTK_INT *rl, CCTK_INT *ii, CCTK_INT *jj, CCTK_INT *kk, CCTK_REAL *x, CCTK_REAL *y, CCTK_REAL *z, CCTK_INT *keytemp, CCTK_REAL *eos_prec, CCTK_REAL *prec,
						  CCTK_REAL *gamma_eos,
						  CCTK_REAL *dens_in,
						  CCTK_REAL *sx_in, CCTK_REAL *sy_in, CCTK_REAL *sz_in,
						  CCTK_REAL *tau_in, CCTK_REAL *Bconsx_in, CCTK_REAL *Bconsy_in, CCTK_REAL *Bconsz_in,
						  CCTK_REAL *y_e_in, CCTK_REAL *temp_in,
						  CCTK_REAL *rho,
						  CCTK_REAL *velx, CCTK_REAL *vely, CCTK_REAL *velz,
						  CCTK_REAL *epsilon, CCTK_REAL *pressure,
						  CCTK_REAL *Bx, CCTK_REAL *By, CCTK_REAL *Bz, CCTK_REAL *bsq,
						  CCTK_REAL *w_lorentz,
						  CCTK_REAL *gxx, CCTK_REAL *gxy, CCTK_REAL *gxz,
						  CCTK_REAL *gyy, CCTK_REAL *gyz, CCTK_REAL *gzz,
						  CCTK_REAL *uxx, CCTK_REAL *uxy, CCTK_REAL *uxz,
						  CCTK_REAL *uyy, CCTK_REAL *uyz, CCTK_REAL *uzz,
						  CCTK_REAL *sdet,
						  CCTK_INT *epsnegative,
						  CCTK_REAL* retval);{
  extern struct c2p_report c2p_rep;
  CCTK_REAL sx,sy,sz;
  CCTK_REAL usx,usy,usz;
  CCTK_REAL tau,dens,gammeos;
  CCTK_REAL BdotS, S_squared, B_squared;
  CCTK_REAL con[NUM_CON], prim[NUM_PRIM];
  CCTK_REAL g_con[3][3];
  CCTK_REAL g_cov[3][3];
  CCTK_REAL sqrt_detg = *sdet;
  CCTK_REAL inv_sqrt_detg = 1./sqrt_detg;
  CCTK_INT i,j,i_increase;

  // set the metric array that we'll pass on for recon
  g_cov[0][0] = *gxx;
  g_cov[1][1] = *gyy;
  g_cov[2][2] = *gzz;
  g_cov[0][1] = g_cov[1][0] = *gxy;
  g_cov[0][2] = g_cov[2][0] = *gxz;
  g_cov[1][2] = g_cov[2][1] = *gyz;
  
  g_con[0][0] = *uxx;
  g_con[1][1] = *uyy;
  g_con[2][2] = *uzz;
  g_con[0][1] = g_cov[1][0] = *uxy;
  g_con[0][2] = g_cov[2][0] = *uxz;
  g_con[1][2] = g_cov[2][1] = *uyz;

  gammaeos = *gamma_eos;

  // undensitize all conservative variables

  sx = ( *sx_in) * inv_sqrt_detg;
  sy = ( *sy_in) * inv_sqrt_detg;
  sz = ( *sz_in) * inv_sqrt_detg;
  tau = ( *tau_in) * inv_sqrt_detg;
  dens = ( *dens_in) * inv_srt_detg;
  usx = g_con[0][0]*sx + g_con[0][1]*sy + g_con[0][2]*sz;
  usy = g_con[1][0]*sx + g_con[1][1]*sy + g_con[1][2]*sz;
  usz = g_con[2][0]*sz + g_con[2][1]*sy + g_con[2][2]*sz;

  *Bx = (*Bconsx_in) * inv_sqrt_detg;
  *By = (*Bconsy_in) * inv_sqrt_detg;
  *Bz = (*Bconsz_in) * inv_sqrt_detg;

  // Now we need some scalars

  B_sqared = g_cov[0][0] * (*Bx) * (*Bx)+
    g_cov[1][1] * (*By) * (*By) +
    g_cov[2][2] * (*Bz) * (*Bz) +
    2 * ( g_cov[0][1] * (*Bx) * (*By) +
	  g_cov[0][2] * (*Bx) * (*Bz) +
	  g_cov[1][2] * (*By) * (*Bz) );

  S_squared = sx*usx + sy*usy + sz*usz;
  BdotS = sx*(*Bx) + sy*(*By) * sz*(*Bz) ;   // I can just hope that B has contravariant indices!!
  
  // we need to add something that checks for NaNs !

  // Compute covariant components of v and Lorentz factor! 
  CCTK_REAL velx_cov, vely_cov, velz_cov;
  CCTK_REAL vsq, gamma,  W_in;

  velx_cov = g_cov[0][0] * (*velx) + g_cov[0][1] * (*vely) + g_cov[0][2] * (*velz) ;
  vely_cov = g_cov[1][0] * (*velx) + g_cov[1][1] * (*vely) + g_cov[1][2] * (*velz) ;
  velz_cov = g_cov[2][0] * (*velx) + g_cov[2][1] * (*vely) + g_cov[2][2] * (*velz) ;

  vsq = (*velx)*velx_cov + (*vely)*vely_cov + (*velz)*velz_cov ;
  gamma = sqrt(1-vsq);
  W_in = 1./gamma;
  
						  
  // setup primitive variables in the format taken by the recon algorithm 
  prim[0] = *rho;
  prim[1] = velx_cov;
  prim[2] = vely_cov;
  prim[3] = velz_cov;
  prim[4] = *epsilon;
  prim[5] = (*Bx);
  prim[6] = (*By);
  prim[7] = (*Bz);
  prim[8] = *y_e_in; 
  prim[9] = *temp_in;
  prim[10] = *pressure;
  prim[11] = W_in;
  // set up conservative variables in the format taken by the recon algorithm
  con[0] = *dens_in;
  con[1] = *sx_in;
  con[2] = *sy_in;
  con[3] = *sz_in;
  con[4] = *tau_in;
  con[5] = *Bx; // should these be the same or b? need to check! 
  con[6] = *By;
  con[7] = *Bz;
  con[8] = *y_e_in;
  // I don't think we need T and P in the con variables ! 

  // set a temporary variable with old primitives in case c2p fails and we need to use the backup method
  CCTK_REAL oldprim[NUM_PRIM];

  for(i=0,i<NUM_PRIM,i++){
    oldprim[i] = prim[i];
  };

  // now we need to setup c2p_steer and c2p_rep and then we're all set...
};
						  
						


