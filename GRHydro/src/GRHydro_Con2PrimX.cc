/* 
*(c) 2021 Carlo Musolino
*/

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "cctk_Functions.h"
#include "SpaceMask.h"
//#include "GRHydro_Macros.h"
#include "GRHydro_Con2Prim.h"


/* 
* These macros were taken from file GRHydro_Con2PrimM_pt.c
* by S.C. Noble C.F. Gammie J.C. McKinney L. Del Zanna 
* [1] Noble, S. C., Gammie, C. F., McKinney, J. C., \& Del Zanna, L. 
*     \ 2006, Astrophysical Journal, 641, 626.
* available with the standard EinsteinToolkit_2020_05 distribution
*/
#define MAX_NEWT_ITER (50)
#define NEWT_TOL (1.0e-10)
#define MIN_NEWT_TOL (1.0e-08)
#define EXTRA_NEWT_ITER  (2)

#define NEWT_TOL2 (1.0e-15)
#define MIN_NEWT_TOL2 (1.0e-10)

#define DEBUG_CON2PRIM (0) // set to 1 for debug output 


#define NUM_PRIM (12)
#define NUM_CON  (9)

/*
* Honestly, I believe no one ever uses these anyway
*/

#define EOS_EPS_MAX (1e05)
#define EOS_PRESS_MAX (1e03)

/*****************************************************************************************************


New cxx Con2Prim routine based on Siegel et al. arXiv:1712.07538v 3D NR algorithm.
Designed to work with tabulated Nuclear EOSs in HDF5 format. Note that this requires 
the updated EOS_Omni aliased interface to work properly (dpdrhot dpdtrho dedrhot dedtrho)

*****************************************************************************************************/




static inline void UpperMetricCX(CCTK_REAL g_con[3][3],
				const CCTK_REAL det,
				const CCTK_REAL g_cov[3][3])
{
  const CCTK_REAL invdet = 1.0 / det;
  g_con[0][0] = (-g_cov[1][2]*g_cov[1][2] + g_cov[1][1]*g_cov[2][2])*invdet;
  g_con[0][1] = g_con[1][0] = (g_cov[0][2]*g_cov[1][2] - g_cov[0][1]*g_cov[2][2])*invdet;
  g_con[0][2] = g_con[2][0] = (-g_cov[0][2]*g_cov[1][1] + g_cov[0][1]*g_cov[1][2])*invdet;
  g_con[1][1] = (-g_cov[0][2]*g_cov[0][2] + g_cov[0][0]*g_cov[2][2])*invdet;
  g_con[1][2] = g_con[2][1] = (g_cov[0][1]*g_cov[0][2] - g_cov[0][0]*g_cov[1][2])*invdet;
  g_con[2][2] = (-g_cov[0][1]*g_cov[0][1] + g_cov[0][0]*g_cov[1][1])*invdet;

  return;
}

/*
* When converting cons to prim variables we use the updated metric tensor but
* the prim initial guesses come from last timestep. This means we could break
* the v^2<=1 constraint. The following function resets v so that it has a physical
* value. Algorithm taken from Siegel's Con2Prim package available at:
* https://bitbucket.org/dsiegel/grmhd_con2prim/src/master/
*/


void reset_v(CCTK_REAL g_con[3][3],CCTK_REAL prim[NUM_PRIM])
{

  CCTK_REAL velx = g_con[0][0] * prim[v1_cov] + g_con[0][1] * prim[v2_cov] + g_con[0][2] * prim[v3_cov] ;
  CCTK_REAL vely = g_con[0][1] * prim[v1_cov] + g_con[1][1] * prim[v2_cov] + g_con[1][2] * prim[v3_cov] ;
  CCTK_REAL velz = g_con[0][2] * prim[v1_cov] + g_con[1][2] * prim[v2_cov] + g_con[2][2] * prim[v3_cov] ;

  CCTK_REAL v_sq = velx * prim[v1_cov] + vely * prim[v2_cov] + velz * prim[v3_cov] ; 

  if (v_sq >= 1.0) {

    prim[v1_cov] *= 1.0 / (v_sq*(1.0+1.0e-10)) ;
    prim[v2_cov] *= 1.0 / (v_sq*(1.0+1.0e-10)) ;
    prim[v3_cov] *= 1.0 / (v_sq*(1.0+1.0e-10)) ; 

    velx = g_con[0][0] * prim[v1_cov] + g_con[0][1] * prim[v2_cov] + g_con[0][2] * prim[v3_cov] ;
    vely = g_con[0][1] * prim[v1_cov] + g_con[1][1] * prim[v2_cov] + g_con[1][2] * prim[v3_cov] ;
    velz = g_con[0][2] * prim[v1_cov] + g_con[1][2] * prim[v2_cov] + g_con[2][2] * prim[v3_cov] ;

    v_sq = velx * prim[v1_cov] + vely * prim[v2_cov] + velz * prim[v3_cov] ; 

    prim[WLORENTZ] = 1.0 / sqrt(1.0-v_sq) ; 

  }
  else if (v_sq < 0.0) {
    prim[v1_cov] = 0.0;
    prim[v2_cov] = 0.0;
    prim[v3_cov] = 0.0;
    prim[WLORENTZ] = 1.0;
  }

} // end of reset_v



/* 
* Main routine: 
* This is what the scheduler will call
*/

void Conservative2PrimitiveX (CCTK_ARGUMENTS) {

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;


  CCTK_INT i,j,k,nx,ny,nz;
  CCTK_REAL g_con[3][3], g_cov[3][3], sdet;
  CCTK_REAL velx,vely,velz,sx,sy,sz;
  CCTK_REAL BdotS, B_sq, S_sq, v_sq;
  CCTK_REAL prim[NUM_PRIM], con[NUM_CON]; //these macros should be set via parameters, for now they're just fixed in the header file...

  // EOS_Omni vars
  CCTK_INT n,keytemp,anyerr,keyerr;
  CCTK_REAL xpress[1],xtemp[1],xye[1],xeps[1],xrho[1];

  char warnline[200];

  // These two structs hold all the relevant steering and report parameters for the c2p functions
  struct c2p_steer *c2p;
  struct c2p_report *c2p_rep;

  c2p->c2p_method        = 0;
  c2p->c2p_method_backup = 1;
  c2p->use_c2p_method_backup = true;
  c2p->eoskey           = *GRHydro_eos_handle;
  c2p->eoskey_polytrope = *GRHydro_polytrope_handle;
  c2p->eos_prec         = GRHydro_eos_rf_prec;
  c2p->eos_rho_min      = *GRHydro_rho_min;
  c2p->eos_rho_max      = GRHydro_eos_rho_max;
  c2p->eos_temp_min     = GRHydro_hot_atmo_temp;
  c2p->eos_temp_max     = GRHydro_eos_temp_max; // this doesn't exist yet .. 
  c2p->eos_eps_max      = EOS_EPS_MAX;
  c2p->eos_press_max    = EOS_PRESS_MAX;
  c2p->evolve_T         = *evolve_temper;
  c2p->evolve_Ye        = *evolve_Y_e;
  c2p->tol_x            = NEWT_TOL; // these used to be macros, just need to copy them 
  c2p->tol_x_retry      = NEWT_TOL2;
  c2p->max_iterations   = MAX_NEWT_ITER;
  c2p->extra_iterations = EXTRA_NEWT_ITER;
  c2p->rho_atmo         = *GRHydro_rho_min;
  c2p->rho_atmo_tol     = GRHydro_atmo_tolerance;
  c2p->T_atmo           = GRHydro_hot_atmo_temp;
  c2p->Ye_atmo          = GRHydro_hot_atmo_Y_e;
  c2p->retain_B_atmo    = false;
  c2p->numprims         = NUM_PRIM;
  c2p->numcons          = NUM_CON;
  c2p->enforce_v2       = true;





  // lsh(i) is the local (MPI task-bound) gridsize in the ith direction 
  nx = cctkGH->cctk_lsh[0]; //cctk_lsh[0] should also work (?)
  ny = cctkGH->cctk_lsh[1];
  nz = cctkGH->cctk_lsh[2];

  /** initialize EOS vars **/
  *xtemp = 0.0;
  *xye = 0.0; // these are just placeholders for polycalls or whatever
  *xrho = *GRHydro_rho_min;
  n = 1; 
  anyerr = 0;
  keyerr = 0;
  keytemp = 0;
  /************************/

  /*
  * Now we make a few poly EOS calls to determine pmin and epsmin
  */


  CCTK_REAL* pmin;
  CCTK_REAL * epsmin;
  CCTK_REAL* one;

  *one = 1.0;

  EOS_Omni_press(*GRHydro_polytrope_handle, keytemp, GRHydro_eos_rf_prec, n,\
    xrho,one,xtemp,xye,pmin,&keyerr,&anyerr);
  EOS_Omni_EpsFromPress(*GRHydro_polytrope_handle, keytemp, GRHydro_eos_rf_prec,n,\
    xrho,epsmin,xtemp,xye,pmin,epsmin,&keyerr,&anyerr);

  c2p->eos_press_min = *pmin;
  c2p->eos_eps_min   = *epsmin;

/*
* And determine the atmosphere pressure..
*/
  CCTK_REAL* eps_atmo;
  CCTK_REAL * p_atmo;

  if (evolve_temper) {

    keytemp = 1;
    *xtemp = GRHydro_hot_atmo_temp;
    *xye   = GRHydro_hot_atmo_Y_e;
    // I think this function also sets eps ... (would be logical) CHECK!! Also check wether inputs are pointers or variables...
    EOS_Omni_press(*GRHydro_eos_handle,keytemp,GRHydro_eos_rf_prec,n, \ 
                xrho,eps_atmo,xtemp,xye,\
                p_atmo,&keyerr,&anyerr);

    // if EOS_Omni fails here we try calling it again with the minimum EOS temperature
    if (anyerr){

      sprintf(warnline,\
              "EOS_Omni error, unable to determine atmosphere pressure! EOS_Omni keyerr: %d \n Trying with EOS_min_temp...\n",\
              keyerr);

      CCTK_WARN(CCTK_WARN_COMPLAIN,warnline);

      keytemp = 1;
      *xtemp = c2p->eos_temp_min;
      EOS_Omni_press(*GRHydro_eos_handle,keytemp,GRHydro_eos_rf_prec,n, \
      xrho,eps_atmo,xtemp,xye,\
      p_atmo,&keyerr,&anyerr);

      }
      if (anyerr) {  // I think we just have to abort...
              c2p_rep->failed = 1;
      }

   } else {

    *xtemp = 0.0;
    *xye   = 0.0;

    keyerr = 0;
    // make poly calls to determine p_atmo, press_atmo for non-nuclear EOS! 
    EOS_Omni_press(*GRHydro_polytrope_handle, keytemp, GRHydro_eos_rf_prec, n,\
    xrho,epsmin,xtemp,xye,p_atmo,&keyerr,&anyerr);
    EOS_Omni_EpsFromPress(*GRHydro_polytrope_handle, keytemp, GRHydro_eos_rf_prec,n,\
    xrho,eps_atmo,xtemp,xye,p_atmo,eps_atmo,&keyerr,&anyerr);

    if (anyerr){
      c2p_rep->failed = 1;
    }

   } // end of p_atmo, eps_atmo calculation for evolve_temper = 1 and = 0
    


c2p->press_atmo = *p_atmo ; 
c2p->eps_atmo   = *eps_atmo;

keytemp = 0;
anyerr  = 0;
keyerr  = 0;

//before we begin, check if c2p failed already in determining atmo quantities
if (c2p_rep->failed)
{
  // just abort
  sprintf(warnline,\
	  "Error while determining atmosphere quantities!");
  CCTK_ERROR(warnline);
}


  
  // loop over gridpoints !! 

# pragma omp parallel for collapse(3) schedule(dynamic,100)	\
  private(i,j,k,sdet,g_cov,g_con,prim,con,\
  	velx,vely,velz,sx,sy,sz,\
  	BdotS, S_sq, B_sq, v_sq,\
    warnline, c2p_rep) \
  firstprivate(xtemp,xye,xrho,n,anyerr,keyerr,keytemp)
 for (i=0;i<nx;i++){
   for(j=0;j<ny;j++){
     for(k=0;k<nz;k++){

       
        CCTK_INT l,itracer;
        const CCTK_INT ind3d = CCTK_GFINDEX3D(cctkGH,i,j,k);  

        c2p_rep->failed      = 0;
        c2p_rep->adjust_cons = false;

  			/*
        *
        * Now we set up all the variables we need for c2p
  	    *
        */ 

  			//metric determinant (sqrt)
 
  			sdet = sdetg[ind3d]; 

  			// covariant metric

  			g_cov[0][0] = gxx[ind3d];
  			g_cov[1][1] = gyy[ind3d];
  			g_cov[2][2] = gzz[ind3d];
  			g_cov[0][1] = g_cov[1][0] = gxy[ind3d];
  			g_cov[0][2] = g_cov[2][0] = gxz[ind3d];
  			g_cov[1][2] = g_cov[2][1] = gyz[ind3d];

  			//contravariant metric (function defined in GRHydro_Sources.cc)

  			UpperMetricCX(g_con,sdet*sdet,g_cov);

  			/****************
  			primitive variables (last evolution step)
  			*****************/

  			prim[RHO] = rho[ind3d];

  			//velocity 

  			velx = vel[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,0)];
  			vely = vel[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,1)];
  			velz = vel[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,2)];

  			prim[v1_cov] = g_cov[0][0]*velx + g_cov[0][1]*vely + g_cov[0][2]*velz ;
  			prim[v2_cov] = g_cov[0][1]*velx + g_cov[1][1]*vely + g_cov[1][2]*velz ;
  			prim[v3_cov] = g_cov[0][2]*velx + g_cov[1][2]*vely + g_cov[2][2]*velz ;

  			v_sq = velx * prim[v1_cov] + vely * prim[v2_cov] + velz * prim[v3_cov];

  			prim[EPS] = eps[ind3d] ; 

  			prim[B1_con] = Bvec[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,0)];
  			prim[B2_con] = Bvec[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,1)];
  			prim[B3_con] = Bvec[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,2)];

  			if (evolve_Y_e) prim[YE] = Y_e[ind3d];
        else prim[YE] = *xye; // just set to something (this is a pointer for some reason...)

  			if (evolve_temper) prim[TEMP] = temperature[ind3d];
  			else prim[TEMP] = *xtemp; // just set to something (for some reason I made this a pointer too!)

  			prim[PRESS] = press[ind3d];

  			prim[WLORENTZ] = 1. / sqrt(1.0 - v_sq) ;

  			/****************
  			conserved variables 
  			*****************/

  			con[D] = dens[ind3d];

  			// Conserved momenta  

  			sx = scon[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,0)];
  			sy = scon[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,1)];
  			sz = scon[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,2)];

  			con[S1_cov] = g_cov[0][0]*sx + g_cov[0][1]*sy + g_cov[0][2]*sz ;
  			con[S2_cov] = g_cov[0][1]*sx + g_cov[1][1]*sy + g_cov[1][2]*sz ;
  			con[S3_cov] = g_cov[0][2]*sx + g_cov[1][2]*sy + g_cov[2][2]*sz ;


  			con[TAU] = tau[ind3d];

  			// magfield (not actually sure this is right, at all, help?)
  			con[B1_con] = prim[B1_con];
  			con[B2_con] = prim[B2_con];
  			con[B3_con] = prim[B3_con];

  			con[YE] = Y_e_con[ind3d];


        // Compute the scalars we'll need for Con2Prim

        B_sq = g_cov[0][0] * con[B1_con] * con[B1_con] +\ 
        g_cov[1][1] * con[B2_con] * con[B2_con] +\ 
        g_cov[2][2] * con[B3_con] * con[B3_con] +\
        + 2* ( g_cov[0][1] * con[B1_con] * con[B2_con] +\ 
          g_cov[0][2] * con[B1_con] * con[B3_con] +\ 
          g_cov[1][2] * con[B2_con] * con[B3_con] );

        S_sq = sx * con[S1_cov] + sy * con[S2_cov] + sz * con[S3_cov] ;

        BdotS = con[B1_con] * con[S1_cov] + con[B2_con] * con[S2_cov] + con[B3_con] * con[S3_cov] ; 
 
  			// now we need to check if we're in the atmosphere...

  			if(atmosphere_mask[ind3d]) {

  				// Do nothing
  				 
  				continue;

  			}
        // TODO: Tracer treatment is not yet implemented (2021-04-11)
        /*if (evolve_tracer) {
          for (itracer=0,itracer<number_of_tracers,itracer++)
          {
            Con2Prim_pt_Tracer(cons_tracer[ind3d][itracer],tracer[ind3d][itracer],\
              prim[D]);
            if (use_min_tracer) {
             tracer[ind3d][itracer] = MIN(tracer[ind3d][itracer],min_tracer);
            } 
          }
        } TRACERS ARE NOT YET SUPPORTED! */
        if (evolve_tracer) {
          sprintf(warnline,"Tracer Con2Prim is not yet supported!");
          CCTK_WARN(CCTK_WARN_ALERT,warnline);
        } 

        if(evolve_Y_e){ // Check that Y_e is within bounds! 
          Y_e[ind3d] = MAX(MIN(Y_e_con[ind3d]/con[D],GRHydro_Y_e_max),GRHydro_Y_e_min);
        }

  			// Do we need to reset the point to atmosphere?

  			if( ((GRHydro_enable_internal_excision != 0) && (hydro_excision_mask[ind3d]))|| \
        (con[D] < sdet * c2p->rho_atmo * (1.0+c2p->rho_atmo_tol)) ) 
  			{
				//reset to atmo	...
          dens[ind3d] = sdet * c2p->rho_atmo;
          rho[ind3d]  = c2p->rho_atmo;
          scon[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,0)] = 0.0;
          scon[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,1)] = 0.0;
          scon[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,2)] = 0.0;
          vel[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,0)]  = 0.0;
          vel[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,1)]  = 0.0;
          vel[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,2)]  = 0.0;
          w_lorentz[ind3d]                  = 1.0;

          Bvec[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,0)] = 0.0;
          Bvec[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,1)] = 0.0;
          Bvec[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,2)] = 0.0;
          Bcons[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,0)] = 0.0;
          Bcons[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,1)] = 0.0;
          Bcons[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,2)] = 0.0;

          if(evolve_temper){
            temperature[ind3d] = GRHydro_hot_atmo_temp;
            Y_e[ind3d]         = GRHydro_hot_atmo_Y_e;
            Y_e_con[ind3d]     =  Y_e[ind3d] * dens[ind3d];
            press[ind3d]       = c2p->press_atmo;
            eps[ind3d]         = c2p->eps_atmo;

          }
          else { 
            keytemp = 0;
            press[ind3d] = c2p->press_atmo;
            eps[ind3d]   = c2p->eps_atmo;

            if(anyerr) {
              c2p_rep->failed = 1;
            }

          }

          tau[ind3d] = sdet * (rho[ind3d]*(1.0+eps[ind3d])) - dens[ind3d];

          if(tau[ind3d] < 0){
            tau[ind3d] = *GRHydro_tau_min;
          }

          c2p_rep->adjust_cons = true;

          continue;

  			} // end of reset to atmo


      /*
      Now the real Con2Prim work begins...
      We consider the interesting case (where we use a full nuclear EOS and evolve T) first.
      */ 

        if(evolve_temper) {

          /*
          * Store last t-step primitive variables
          * in case c2p fails and we need a backup
          */
          CCTK_REAL oldprims[NUM_PRIM];

          for (l=0;l<NUM_PRIM;l++)
          {
            oldprims[l] = prim[l];
          }

        
          /* 
          * The metric we use is from this step but velocities
          * come from last step, we need to enforce 0 <= v_sq <= 1
          * This also handles W_LORENTZ.
          */
          reset_v(g_con,prim);

          NR_3D(c2p_rep, S_sq, BdotS, B_sq,\
            con, prim, g_con, g_cov,\ 
		c2p->tol_x,0,1,c2p); // TODO: code needs to be modified so we pass c2p in as well ! 

          if(c2p_rep->failed){
            for (l=0;l<NUM_PRIM;l++)
            {
              prim[l] = oldprims[l];
            }
            // we're trying with Cerda Duran's "safe guess"
            NR_3D(c2p_rep, S_sq, BdotS, B_sq,\
            con, prim, g_con, g_cov,\ 
		  c2p->tol_x,1,1,c2p); // TODO: code needs to be modified so we pass c2p in as well ! AND WE NEED TO CHANGE THE METRIC FORMAT  
          }

          if(c2p_rep->failed){
            if(c2p->use_c2p_method_backup){
              for (l=0;l<NUM_PRIM;l++)
              {
                prim[l] = oldprims[l];
              }
            newman(c2p_rep,S_sq,BdotS,B_sq,\
		   con, prim,g_cov,g_con,c2p); // TODO: code needs to be modified so we pass c2p in as well ! 
            }
          } // end of backup c2p call

        } else {
          // this is the case where we don't evolve T
          if (*GRHydro_eos_handle < 4 ){
            newman(c2p_rep,S_sq,BdotS,B_sq,\
		   con, prim,g_cov,g_con,c2p); // TODO: code needs to be modified so we pass c2p in as well ! 
          }
          /*else if (GRHydro_eos_handle == 4){
            // Can someone not evolve T and still use a nuclear eos??
          }*/
          else {
             sprintf(warnline,"EOS_Handle %d not supported without Temperature evolution!",GRHydro_eos_handle);
             CCTK_WARN(CCTK_WARN_ABORT,warnline);
          }
        }


        /****************************************************
        * Now check if c2p has converged and abort otherwise
        *****************************************************/

        if(c2p_rep->failed) {
            // now we need to abort and set the mask
            // who knows what GRHydro does with it
            GRHydro_C2P_failed[ind3d] = 1;
            // abort (should we try and set the point to atmo?)
            sprintf(warnline,\
              "Conservative2Primitive failed at point i j k %d %d %d \n \
              temp y_e: %1.5e %1.5e \n \
              keyerr: %d",i,j,k,prim[TEMP],prim[YE],keyerr);
            CCTK_WARN(CCTK_WARN_ALERT,warnline);
          }
          else {

          /*******************************************
          *
          * If we get here we're out of the woods and 
          * we just have to write back the new primitive
          * variables
          *
          ********************************************/
          rho[ind3d] = prim[RHO];

          vel[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,0)] = g_con[0][0] * prim[v1_cov]  + g_con[0][1] * prim[v2_cov] + g_con[0][2] * prim[v3_cov];
          vel[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,1)] = g_con[0][1] * prim[v1_cov]  + g_con[1][1] * prim[v2_cov] + g_con[1][2] * prim[v3_cov];
          vel[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,2)] = g_con[0][2] * prim[v1_cov]  + g_con[1][2] * prim[v2_cov] + g_con[2][2] * prim[v3_cov];


          Bvec[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,0)] = prim[B1_con];
          Bvec[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,1)] = prim[B2_con];
          Bvec[CCTK_VECTGFINDEX3D(cctkGH,i,j,k,2)] = prim[B3_con];

          eps[ind3d] = prim[EPS];
          press[ind3d] = prim[PRESS];

          if (evolve_temper) temperature[ind3d] = prim[TEMP] ;
          if (evolve_Y_e) Y_e[ind3d] = prim[YE] ; 
        }

  		}
  	}
  } // end of parallel loops over gridpoints

} // end of c2p routine 



