#ifndef con2prim
#define con2prim

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>

#define DEBUG 0

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

#define RHO 0
#define v1_cov 1
#define v2_cov 2
#define v3_cov 3
#define EPS 4

#define D 0
#define S1_cov 1
#define S2_cov 2
#define S3_cov 3
#define TAU 4

#define B1_con 5
#define B2_con 6
#define B3_con 7

#define YE 8
#define TEMP 9
#define PRESS 10
#define WLORENTZ 11
#define ENT 12
#define A_BAR 13


// no one uses this ? ?
struct metric {
    double lo[4][4];
    double up[4][4];
    double lo_det;
    double up_det;
    double lo_sqrt_det;
};


struct c2p_steer {
  int c2p_method;
  int c2p_method_backup;
  bool use_c2p_method_backup;
  int eoskey;
  int eoskey_polytrope;
  double eos_prec;
  double eos_rho_min;
  double eos_rho_max;
  double eos_temp_min;
  double eos_temp_max;
  double eos_eps_min;
  double eos_eps_max;
  double eos_press_min;
  double eos_press_max;
  int evolve_T;
  int evolve_Ye;
  double tol_x;
  double tol_x_retry;
  int max_iterations;
  int extra_iterations;
  double rho_atmo;
  double rho_atmo_tol;
  double T_atmo;
  double Ye_atmo;
  bool retain_B_atmo;
  int numprims;
  int numcons;
  bool enforce_v2;
  double press_atmo;
  double eps_atmo;
};


struct c2p_report {
  bool failed;
  bool adjust_cons;
  char err_msg[200];
  int count;
  bool retry;
  int c2p_keyerr;
};


// functions
                  
void NR_3D(struct c2p_report * c2p_rep, const double S_squared, const double BdotS, 
  const double B_squared, const double * con, double * prim, const double g_con[3][3], 
	   const double g_cov[3][3], const double tol_x, int SAFEGUESS, int stepsize, struct c2p_steer * c2p);
    

void newman(struct c2p_report * c2p_rep, const double S_squared, 
          const double BdotS, const double B_squared, const double * con, double * prim, 
	    const double g_cov[3][3], const double g_con[3][3],  struct c2p_steer * c2p);

  double P_from_prim(double * prim, struct c2p_steer * c2p);

  void EOSprim_from_rhoTYe(const int keytemp, double * prim, struct c2p_steer * c2p);

void EOS_press(int eoskey, int keytemp, double rho, double * eps, 
	       double * temp, double ye, double * prs, int * keyerr, struct c2p_steer* c2p);

void EOS_eps_from_press(int eoskey, double rho, double * eps, 
			double * temp, double ye, double prs, int * keyerr, const double prec);

void EOS_EP_dEdr_dEdt_dPdr_dPdt(double * x, const double * con, double * Eprim, 
				double * Pprim, double * dEdrho, double * dEdt, double * dPdrho, double * dPdt, int stepsize, const double prec, const int eos_key);

/*void EOS_EP_dEdr_dEdt_dPdr_dPdt_2D(const double rho2D, const double temp2D, const double * con, double * Eprim, 
    double * Pprim, double * dEdrho, double * dEdt, double * dPdrho, double * dPdt, int stepsize);*/

void EP_dEdW_dEdZ_dEdT(double * Eprim, double * Pprim, double * dEdW, double * dEdZ, double * dEdT, 
		       double * dpdrho, double * dpdT, double * x, const double * con, int stepsize, const int eos_key, const double prec);
    
void EP_dPdW_dPdZ_dPdT(double * Eprim, double * Pprim, double * dPdW, double * dPdZ, double * dPdT, 
		       double * dpdrho, double * dpdT, double * x, const double * con, int stepsize, const int eos_key,const double prec);    

void EOS_press_from_rhoenthalpy(int eoskey, int keytemp, double prec, double rho, double * eps, 
				double * temp, double ye, double * press, double * enth, int * anyerr, int * keyerr);


#ifdef __cplusplus
}
#endif

#endif
