#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

void ElectroBase_Evec_zero(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS_ElectroBase_Evec_zero;
  DECLARE_CCTK_PARAMETERS;

  int const np = cctk_ash[0] * cctk_ash[1] * cctk_ash[2];

#pragma omp parallel for
  for (int i = 0; i < np; ++i) {
    Evec[i] = 0.0;
    Evec[i + np] = 0.0;
    Evec[i + 2 * np] = 0.0;
  }

  if (CCTK_EQUALS(initial_data_setup_method, "init_some_levels") ||
      CCTK_EQUALS(initial_data_setup_method, "init_single_level")) {
    /* do nothing */
  } else if (CCTK_EQUALS(initial_data_setup_method, "init_all_levels")) {

    if (CCTK_ActiveTimeLevels(cctkGH, "ElectroBase::Evec") >= 2) {
#pragma omp parallel for
      for (int i = 0; i < np; ++i) {
        Evec_p[i] = 0.0;
        Evec_p[i + np] = 0.0;
        Evec_p[i + 2 * np] = 0.0;
      }
    }

    if (CCTK_ActiveTimeLevels(cctkGH, "ElectroBase::Evec") >= 3) {
#pragma omp parallel for
      for (int i = 0; i < np; ++i) {
        Evec_p_p[i] = 0.0;
        Evec_p_p[i + np] = 0.0;
        Evec_p_p[i + 2 * np] = 0.0;
      }
    }

    if (CCTK_ActiveTimeLevels(cctkGH, "ElectroBase::Evec") >= 4) {
      CCTK_WARN(CCTK_WARN_ABORT,
                "Too many active time levels for ElectroBase::Evec");
    }

  } else {
    CCTK_WARN(
        CCTK_WARN_ABORT,
        "Unsupported parameter value for InitBase::initial_data_setup_method");
  }
}

void ElectroBase_Jvec_zero(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS_ElectroBase_Jvec_zero;
  DECLARE_CCTK_PARAMETERS;

  int const np = cctk_ash[0] * cctk_ash[1] * cctk_ash[2];

#pragma omp parallel for
  for (int i = 0; i < np; ++i) {
    Jvec[i] = 0.0;
    Jvec[i + np] = 0.0;
    Jvec[i + 2 * np] = 0.0;
  }

  if (CCTK_EQUALS(initial_data_setup_method, "init_some_levels") ||
      CCTK_EQUALS(initial_data_setup_method, "init_single_level")) {
    /* do nothing */
  } else if (CCTK_EQUALS(initial_data_setup_method, "init_all_levels")) {

    if (CCTK_ActiveTimeLevels(cctkGH, "ElectroBase::Jvec") >= 2) {
#pragma omp parallel for
      for (int i = 0; i < np; ++i) {
        Jvec_p[i] = 0.0;
        Jvec_p[i + np] = 0.0;
        Jvec_p[i + 2 * np] = 0.0;
      }
    }

    if (CCTK_ActiveTimeLevels(cctkGH, "ElectroBase::Jvec") >= 3) {
#pragma omp parallel for
      for (int i = 0; i < np; ++i) {
        Jvec_p_p[i] = 0.0;
        Jvec_p_p[i + np] = 0.0;
        Jvec_p_p[i + 2 * np] = 0.0;
      }
    }

    if (CCTK_ActiveTimeLevels(cctkGH, "ElectroBase::Jvec") >= 4) {
      CCTK_WARN(CCTK_WARN_ABORT,
                "Too many active time levels for ElectroBase::Jvec");
    }

  } else {
    CCTK_WARN(
        CCTK_WARN_ABORT,
        "Unsupported parameter value for InitBase::initial_data_setup_method");
  }
}

void ElectroBase_Echarge_zero(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS_ElectroBase_Echarge_zero;
  DECLARE_CCTK_PARAMETERS;

  int const np = cctk_ash[0] * cctk_ash[1] * cctk_ash[2];

#pragma omp parallel for
  for (int i = 0; i < np; ++i) {
    Echarge[i] = 0.0;
  }

  if (CCTK_EQUALS(initial_data_setup_method, "init_some_levels") ||
      CCTK_EQUALS(initial_data_setup_method, "init_single_level")) {
    /* do nothing */
  } else if (CCTK_EQUALS(initial_data_setup_method, "init_all_levels")) {

    if (CCTK_ActiveTimeLevels(cctkGH, "ElectroBase::Echarge") >= 2) {
#pragma omp parallel for
      for (int i = 0; i < np; ++i) {
        Echarge_p[i] = 0.0;
      }
    }

    if (CCTK_ActiveTimeLevels(cctkGH, "ElectroBase::Echarge") >= 3) {
#pragma omp parallel for
      for (int i = 0; i < np; ++i) {
        Echarge_p_p[i] = 0.0;
      }
    }

    if (CCTK_ActiveTimeLevels(cctkGH, "ElectroBase::Echarge") >= 4) {
      CCTK_WARN(CCTK_WARN_ABORT,
                "Too many active time levels for ElectroBase::Echarge");
    }

  } else {
    CCTK_WARN(
        CCTK_WARN_ABORT,
        "Unsupported parameter value for InitBase::initial_data_setup_method");
  }
}

void ElectroBase_eta_ohm_zero(CCTK_ARGUMENTS) {
  DECLARE_CCTK_ARGUMENTS_ElectroBase_eta_ohm_zero;
  DECLARE_CCTK_PARAMETERS;

  int const np = cctk_ash[0] * cctk_ash[1] * cctk_ash[2];

#pragma omp parallel for
  for (int i = 0; i < np; ++i) {
    eta_ohm[i] = 0.0;
  }

  if (CCTK_EQUALS(initial_data_setup_method, "init_some_levels") ||
      CCTK_EQUALS(initial_data_setup_method, "init_single_level")) {
    /* do nothing */
  } else if (CCTK_EQUALS(initial_data_setup_method, "init_all_levels")) {

    if (CCTK_ActiveTimeLevels(cctkGH, "ElectroBase::eta_ohm") >= 2) {
#pragma omp parallel for
      for (int i = 0; i < np; ++i) {
        eta_ohm_p[i] = 0.0;
      }
    }

    if (CCTK_ActiveTimeLevels(cctkGH, "ElectroBase::eta_ohm") >= 3) {
#pragma omp parallel for
      for (int i = 0; i < np; ++i) {
        eta_ohm_p_p[i] = 0.0;
      }
    }

    if (CCTK_ActiveTimeLevels(cctkGH, "ElectroBase::eta_ohm") >= 4) {
      CCTK_WARN(CCTK_WARN_ABORT,
                "Too many active time levels for ElectroBase::eta_ohm");
    }

  } else {
    CCTK_WARN(
        CCTK_WARN_ABORT,
        "Unsupported parameter value for InitBase::initial_data_setup_method");
  }
}


