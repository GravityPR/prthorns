/*******************************************
 ****** HEADER FOR THORN MIR_INITDATA ******
 *******************************************/
#ifndef MIR_INITDATA_HEADER_H
#define MIR_INITDATA_HEADER_H


/* ----------- CCTK HEADERS ------------- */
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Functions.h"


#endif
