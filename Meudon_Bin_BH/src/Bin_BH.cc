#include <cassert>
#include <cmath>
#include <cstdio>
#include <vector>
#include <ios>

#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

// LORENE headers
#include<bin_bh.h>
#include "unites.h"

using namespace std;


// define namespace here for old versions of Lorene that don't do so
namespace Lorene {}
using namespace Lorene;


static void set_dt_from_domega (CCTK_ARGUMENTS,
                                CCTK_REAL const* const var,
                                CCTK_REAL      * const dtvar,
                                CCTK_REAL const& omega)
{
  DECLARE_CCTK_ARGUMENTS;
  
  int const npoints = cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2];
  vector<CCTK_REAL> dxvar(npoints), dyvar(npoints);
  
  Diff_gv (cctkGH, 0, var, &dxvar[0], -1);
  Diff_gv (cctkGH, 1, var, &dyvar[0], -1);
  
#pragma omp parallel for
  for (int i=0; i<npoints; ++i) {
    CCTK_REAL const ephix = +y[i];
    CCTK_REAL const ephiy = -x[i];
    CCTK_REAL const dphi_var = ephix * dxvar[i] + ephiy * dyvar[i];
    dtvar[i] = omega * dphi_var;
  }
}



extern "C"
void ID_Bin_BH_initialise (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  
  // Meudon data are distributed in SI units (MKSA).  Here are some
  // conversion factors.
  // Be aware: these are the constants Lorene uses. They do differ from other
  // conventions, but they gave the best results in some tests.

  CCTK_REAL const c_light  = Unites::c_si;      // speed of light [m/s]
  CCTK_REAL const G_grav   = Unites::g_si;      // gravitational constant [m^3/kg/s^2]
  CCTK_REAL const M_sun    = Unites::msol_si;   // solar mass [kg]

  // Cactus units in terms of SI units:
  // (These are derived from M = M_sun, c = G = 1, and using 1/M_sun
  // for the magnetic field)
  CCTK_REAL const cactusM = M_sun;
  CCTK_REAL const cactusL = cactusM * G_grav / pow(c_light,2);
  CCTK_REAL const cactusT = cactusL / c_light;

  CCTK_REAL r1;
  CCTK_REAL a_unit; // BH1's radius
  CCTK_INT n_iter;
  

  CCTK_INFO ("Setting up LORENE Bin_BH initial data");
  CCTK_VInfo (CCTK_THORNSTRING, "Reading from file \"%s\"", radius_filename);
  
  try{
  // ------------------------------------------
  //         FIND LORENE'S LENGTH UNIT
  // ------------------------------------------

  ifstream fich(radius_filename);

  // Read last value of r1 to find unit's conversion
  // This sucks, we should implement the new class bin_bh_export which allows to read off a.

  while ( fich >> n_iter >> r1) {
    a_unit = r1;
  };

  } catch (ios::failure e) {
    CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,
                "Could not read initial data from file '%s': %s", filename,e.what());
  } // <-- End of try catch to find a_unit's value 
  
  CCTK_REAL const LOR_to_CU = 1e04 * a_unit / cactusL ; //LORENE's units are in terms of 10km !! 
  
  CCTK_INFO ("Setting up coordinates");
  
  int const npoints = cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2];
  vector<double> xx(npoints), yy(npoints), zz(npoints);
  
#pragma omp parallel for
  for (int i=0; i<npoints; ++i) {
    xx[i] = x[i] / LOR_to_CU;
    yy[i] = y[i] / LOR_to_CU;
    zz[i] = z[i] / LOR_to_CU;
  }
  


  CCTK_VInfo (CCTK_THORNSTRING, "Reading from file \"%s\"", filename);
  
  // added 1 at the end of bin_bh because of unequal mass models
  try {
    Bin_BH bin_bh (npoints, &xx[0], &yy[0], &zz[0], 1, filename, 1);   // this fills the throats in with parabolic interpolation, just needed on initial slice to allow AHFinderDirect to have points inside the horizon that are not garbage 

  CCTK_REAL const omega_CU = bin_bh.omega / a_unit * Unites::f_unit * cactusT ; //originally expressed in LORENE's unit of t / a
  
  CCTK_VInfo (CCTK_THORNSTRING, "Omega [CU]: %g", omega_CU );
  CCTK_VInfo (CCTK_THORNSTRING, "dist [CU]:    %g", bin_bh.dist * LOR_to_CU );
  CCTK_VInfo (CCTK_THORNSTRING, "radius1 [CU]: %g", LOR_to_CU);
  CCTK_VInfo (CCTK_THORNSTRING, "radius2 [CU]: %g", bin_bh.radius2 * 10 ); // radii are stored in LORENE's unit [ 10 km ]
  assert (bin_bh.np == npoints);
  
  
  
  CCTK_INFO ("Filling in Cactus grid points");

  // No Longer needed: K already has covariant indices - CM 
  CCTK_REAL ku[3][3];
  CCTK_REAL k[3][3]; 
  CCTK_REAL g[3][3];
  

#pragma omp parallel for
  for (int i=0; i<npoints; ++i) {

    g[0][0] = bin_bh.g_xx[i] ;
    g[1][1] = bin_bh.g_yy[i] ;
    g[2][2] = bin_bh.g_zz[i] ; 
    g[0][1] = bin_bh.g_xy[i] ;
    g[0][2] = bin_bh.g_xz[i] ;
    g[1][2] = bin_bh.g_yz[i] ;
    g[1][0] = g[0][1] ;
    g[2][0] = g[0][2] ;
    g[2][1] = g[1][2] ;

    
    ku[0][0] = bin_bh.k_xx[i];
    ku[1][1] = bin_bh.k_yy[i];
    ku[2][2] = bin_bh.k_zz[i];
    ku[0][1] = bin_bh.k_xy[i];
    ku[0][2] = bin_bh.k_xz[i];
    ku[1][2] = bin_bh.k_yz[i];
    ku[1][0] = ku[0][1] ;
    ku[2][0] = ku[0][2] ;
    ku[2][1] = ku[1][2] ;

/*
    for ( int a = 0; a < 3; ++a) {
      for ( int b = 0; b < 3; ++b) {
	for (int c = 0; c < 3; ++c) {
	  k[a][b] = double(0);
	  for ( int d = 0; d < 3; ++d) {
	    k[a][b] += g[a][c] * g[b][d] * k[c][d] ;
	  }
	}
      }
    } // <-- end of lowering indices
*/
 
 
    kxx[i] = k[0][0] / LOR_to_CU;
    kxy[i] = k[0][1] / LOR_to_CU;
    kxz[i] = k[0][2] / LOR_to_CU;
    kyy[i] = k[1][1] / LOR_to_CU;
    kyz[i] = k[1][2] / LOR_to_CU;
    kzz[i] = k[2][2] / LOR_to_CU;
      
    alp[i] = bin_bh.nnn[i] ;
    
    betax[i] = bin_bh.beta_x[i] ;
    betay[i] = bin_bh.beta_y[i] ;
    betaz[i] = bin_bh.beta_z[i] ;
    
    gxx[i] = bin_bh.g_xx[i];
    gxy[i] = bin_bh.g_xy[i];
    gxz[i] = bin_bh.g_xz[i];
    gyy[i] = bin_bh.g_yy[i];
    gyz[i] = bin_bh.g_yz[i];
    gzz[i] = bin_bh.g_zz[i];
    
    
    // extinsic curvature has units of [c/a]
    /*
    kxx[i] = bin_bh.k_xx[i] / LOR_to_CU ;
    kxy[i] = bin_bh.k_xy[i] / LOR_to_CU ;
    kxz[i] = bin_bh.k_xz[i] / LOR_to_CU ;
    kyy[i] = bin_bh.k_yy[i] / LOR_to_CU ;
    kyz[i] = bin_bh.k_yz[i] / LOR_to_CU ;
    kzz[i] = bin_bh.k_zz[i] / LOR_to_CU ;
    */

  } // for i
  
  
  
  CCTK_INFO ("Calculating time derivatives of lapse and shift");
  {

    
    // These initial data assume a helical Killing vector field
    
    if (CCTK_EQUALS (initial_dtlapse, "ID_Bin_BH")) {
      set_dt_from_domega (CCTK_PASS_CTOC, alp, dtalp, omega_CU);
    } else if (CCTK_EQUALS (initial_dtlapse, "none")) {
      // do nothing
    } else {
      CCTK_WARN (CCTK_WARN_ABORT, "internal error");
    }
    
    if (CCTK_EQUALS (initial_dtshift, "ID_Bin_BH")) {
      set_dt_from_domega (CCTK_PASS_CTOC, betax, dtbetax, omega_CU);
      set_dt_from_domega (CCTK_PASS_CTOC, betay, dtbetay, omega_CU);
      set_dt_from_domega (CCTK_PASS_CTOC, betaz, dtbetaz, omega_CU);
    } else if (CCTK_EQUALS (initial_dtshift, "none")) {
      // do nothing
    } else {
      CCTK_WARN (CCTK_WARN_ABORT, "internal error");
    }
  }
  
  
  
  CCTK_INFO ("Done.");
  } catch (ios::failure e) {
    CCTK_VWarn (CCTK_WARN_ABORT, __LINE__, __FILE__, CCTK_THORNSTRING,
                "Could not read initial data from file '%s': %s", filename, e.what());
  }
}
