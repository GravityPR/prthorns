#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "nuc_eos.hh"
#include "helpers.hh"

namespace nuc_eos {

extern "C"
void CCTK_FNAME(nuc_eos_m_kt1_dpdrhot_dedrhot) (const int *restrict n_in,
				     const double *restrict rho, 
				     double *restrict temp,
				     const double *restrict ye,
				     double *restrict eps,
				     double *restrict eps2,
				     double *restrict press,
				     double *restrict dpdrho,
				     double *restrict dpdt,
				     double *restrict dedrho,
				     double *restrict dedt,
				     const double *restrict prec,
				     int *restrict keyerr,
				     int *restrict anyerr)
{
  using namespace nuc_eos;
       
  const int n = *n_in ; 
  int keyerr_2; 
  char warnline[200];
  
  
  for (int i = 0; i<n; i++) {
    int keyerr2 = checkbounds(rho[i], temp[i], ye[i]);
    if(keyerr2 != 0) {
      *anyerr = 1;
      sprintf(warnline,"Inside kt1: keyerr: %d ,%g, %g, %g",keyerr2, rho[i], temp[i], ye[i]);
    }
  }

  if(*anyerr) return;

  for (int i = 0; i<n; i++){

    int idx[8];
    double delx,dely,delz;
    const double lr = log(rho[i]);
    const double lt = log(MIN(MAX(temp[i],eos_tempmin),eos_tempmax));
    get_interp_spots(lr,lt,ye[i],&delx,&dely,&delz,idx);
    {
      int iv=0;
      nuc_eos_C_linterp_one_d(idx,delx,dely,delz,&(press[i]),&(dedrho[i]),&(dedt[i]),&(dpdrho[i]),&(dpdt[i]),iv);
      iv = 1;
      nuc_eos_C_linterp_one_d(idx,delx,dely,delz,&(press[i]),&(dedrho[i]),&(dedt[i]),&(dpdrho[i]),&(dpdt[i]),iv);
      
      press[i] = exp(press[i]);
      eps2[i]  = exp(eps[i]) ;
      eps[i]   = exp(eps[i]) - energy_shift;
    }
  } // end of loop over n_in 
  return;
} // end of function
					    


} // end of namespace 
